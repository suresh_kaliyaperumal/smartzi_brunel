package com.smartzi.dto.response;

public class AdminLoginResponse {

    /** The o auth response. */
    private OAuthResponse oAuthResponse;

    /** The user GUID. */
    private String userGUID;

    /** The user id. */
    private Long userId;

    /** The user name. */
    private String userName;

    /** The email id. */
    private String emailId;

    /** The referral code. */
    private String referralCode;

    private Short userType;





    public OAuthResponse getoAuthResponse() {
        return oAuthResponse;
    }

    public void setoAuthResponse(OAuthResponse oAuthResponse) {
        this.oAuthResponse = oAuthResponse;
    }

    public String getUserGUID() {
        return userGUID;
    }

    public void setUserGUID(String userGUID) {
        this.userGUID = userGUID;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public Short getUserType() {
        return userType;
    }

    public void setUserType(Short userType) {
        this.userType = userType;
    }
}
