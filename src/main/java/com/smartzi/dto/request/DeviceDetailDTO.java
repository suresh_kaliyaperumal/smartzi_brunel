package com.smartzi.dto.request;

public class DeviceDetailDTO {

    /**
     *
     */
    private Long deviceId;
    /**
     *
     */
    private String deviceName;
    /**
     *
     */
    private String deviceModel;
    /**
     *
     */
    private short appPlatform;
    /**
     *
     */
    private String appPlatformVersion;
    /**
     *
     */
    private String deviceToken;
    /**
     *
     */
    private String deviceLocale;
    /**
     *
     */
    private short appType;

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public short getAppPlatform() {
        return appPlatform;
    }

    public void setAppPlatform(short appPlatform) {
        this.appPlatform = appPlatform;
    }

    public String getAppPlatformVersion() {
        return appPlatformVersion;
    }

    public void setAppPlatformVersion(String appPlatformVersion) {
        this.appPlatformVersion = appPlatformVersion;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceLocale() {
        return deviceLocale;
    }

    public void setDeviceLocale(String deviceLocale) {
        this.deviceLocale = deviceLocale;
    }

    public short getAppType() {
        return appType;
    }

    public void setAppType(short appType) {
        this.appType = appType;
    }
}
