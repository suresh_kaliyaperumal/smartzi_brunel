package com.smartzi.dto.request;

public class AdminLoginRequest {

    /** The email id. */
    private String emailId;

    /** The password. */
    private String password;

    private String username;

    private boolean b2bFlag;

    public boolean isB2bFlag() {
        return b2bFlag;
    }

    public void setB2bFlag(boolean b2bFlag) {
        this.b2bFlag = b2bFlag;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
