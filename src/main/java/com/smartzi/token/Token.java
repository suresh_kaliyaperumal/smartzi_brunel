package com.smartzi.token;

public interface Token {

    /**
     * Get the Token
     * @return
     */
    String getSmartziApiToken();

   // String getKarhooApiToken();

    /**
     * Insert Token
     */
    void insertSmartziAccessToken();

   // void insertKarhooAccessToken();
}
