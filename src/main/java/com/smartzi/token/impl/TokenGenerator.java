package com.smartzi.token.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.smartzi.dto.request.AdminLoginRequest;
import com.smartzi.dto.request.DeviceDetailDTO;
import com.smartzi.dto.response.AdminLoginResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
public class TokenGenerator {

    /** The rest template. */
    @Autowired
    RestTemplate restTemplate;

    @Value("${smartzi.username}")
    private String userName;

    @Value("${smartzi.password}")
    private String password;

    @Value("${smartzi.base.url}")
    private String smartziBaseURL;

    /** The login url. */
    @Value("${smartzi.admin.login}")
    private String loginURL;
/*
    @Value("${citytransfer.access.username}")
    private String karhooAccessUserName;

    @Value("${citytransfer.access.password}")
    private String karhooAccessPassword;

    @Value("${citytransfer.base.url}")
    private String karhooBaseURL;

    @Value("${citytransfer.admin.login}")
    private String karhooLoginURL;*/

    public String smartziGenerateToken() {

        AdminLoginRequest adminLoginRequest = new AdminLoginRequest();
        AdminLoginResponse adminLoginResponse = null;
      //  DeviceDetailDTO deviceDetailDTO = new DeviceDetailDTO();

        adminLoginRequest.setEmailId(userName);
        adminLoginRequest.setPassword(password);
        adminLoginRequest.setB2bFlag(true);
        System.out.println("UserName **********" + userName);
        System.out.println("PSW **********" + password);
        String adminLoginResponseRest = restTemplate.postForEntity(smartziBaseURL + loginURL, adminLoginRequest, String.class).getBody();
        System.out.println("accesstoken Res**********" + adminLoginResponseRest);
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonObject jsonObj = (JsonObject) parser.parse(adminLoginResponseRest);
        adminLoginResponse = gson.fromJson(jsonObj.get("data"), AdminLoginResponse.class);
        System.out.println("accesstoken Mapper**********" + adminLoginResponse);

        adminLoginResponse.getUserId();
        return adminLoginResponse.getoAuthResponse().getAccess_token();
    }

    /*public String karhooTokenGen() {


        System.out.println("KarhooUserName **********" + karhooAccessUserName);
        System.out.println("KarhooPSW **********" + karhooAccessPassword);
        System.out.println("KArhooURL"+karhooBaseURL+karhooLoginURL);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        Map<String, String> map = new HashMap<>();
        map.put("username", karhooAccessUserName);
        map.put("password", karhooAccessPassword);

        HttpEntity<Map<String, String>> entity = new HttpEntity<>(map, headers);

        String karhooTokenReturn = restTemplate.postForEntity(karhooBaseURL + karhooLoginURL, entity, String.class).getBody();
        System.out.println("accesstoken Res**********" + karhooTokenReturn);
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonObject jsonObj = (JsonObject) parser.parse(karhooTokenReturn);
        OAuthResponse adminLoginResponse = gson.fromJson(jsonObj, OAuthResponse.class);
        System.out.println("accesstoken Mapper**********" + adminLoginResponse);
        return adminLoginResponse.getAccess_token();
    }*/

}
