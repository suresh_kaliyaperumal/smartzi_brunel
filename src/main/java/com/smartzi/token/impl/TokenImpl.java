package com.smartzi.token.impl;


import com.brunel.constants.ApplicationConstant;
import com.brunel.entity.TokenEntity;
import com.brunel.repository.AdminRepository;
import com.smartzi.token.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling


public class TokenImpl implements Token {

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    TokenGenerator tokenGenerator;

    @Override
    public String getSmartziApiToken() {

        return adminRepository.getSmartziAccessToken(ApplicationConstant.SMARTZI_SUPPLIERID);
    }

   /* @Override
    public String getKarhooApiToken() {
        return adminRepository.getSmartziAccessToken(ApplicationConstant.KARHOO_ACCESS_ID);
    }*/

  //  @Scheduled(fixedRate = 1800000, initialDelay = 20)
    @Scheduled(fixedRate = 1800000)
    @Override
    public void insertSmartziAccessToken() {
        TokenEntity tokenEntity = new TokenEntity();
        tokenEntity.setAccessToken(tokenGenerator.smartziGenerateToken());
        tokenEntity.setSupplierId(ApplicationConstant.SMARTZI_SUPPLIERID);
        adminRepository.insertAccessToken(tokenEntity);

    }
   /* @Scheduled(fixedRate = 1800000)
    @Override
    public void insertKarhooAccessToken() {
        TokenEntity tokenEntity = new TokenEntity();
        tokenEntity.setAccessToken(tokenGenerator.karhooTokenGen());
        tokenEntity.setSupplierId(ApplicationConstant.KARHOO_ACCESS_ID);
        adminRepository.insertAccessToken(tokenEntity);
    }*/

}
