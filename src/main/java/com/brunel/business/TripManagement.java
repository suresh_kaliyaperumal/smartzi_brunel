package com.brunel.business;

import com.brunel.dto.request.BrunelBookingB1Request;
import com.brunel.dto.request.CancelRequest;
import com.brunel.dto.request.InventoryBookingViewQuoteRequest;
import com.brunel.dto.response.*;


public interface TripManagement {

    /**
     * View Quote
     * @param viewQuotesRequest
     * @return
     */
    BrunelViewQuoteResponse getInventoryQuote(BrunelBookingB1Request viewQuotesRequest) ;

    /**
     * Cancel Booking
     * @param cancelRequest, id
     * @return
     */
    String engineCancelBooking(String id, CancelRequest cancelRequest);

    /**
     * Create Booking
     * @param bookingRequest
     * @return
     */

    BrunelBookingResponse engineBooking(BrunelBookingB1Request bookingRequest);

    /**
     * Trip Status view
     * @param tripId
     * @return
     */
    BrunelBookingStatus engineTripStatus(String tripId);

    /**
     * Get Trip Detail
     * @param id
     * @return
     */
    RideDetailsResponse engineTripDetail(String id);
}
