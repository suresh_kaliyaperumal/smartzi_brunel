package com.brunel.business.impl;

import com.brunel.business.RestCallManagement;
import com.smartzi.token.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestCallManagementImpl implements RestCallManagement {

    @Autowired
    Token token;

    @Autowired
    RestTemplate restTemplate;

    @Value("${smartzi.base.url}")
    private String SmartziBaseURL;

   /* @Value("${karhoo.base.url}")
    private String karhooBaseURL;*/



    @Override
    public String getSmartziWebService(Object object, String requestUrl, boolean isAuthRequired) {

        ResponseEntity<String> result = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        System.out.println("baseURL"+SmartziBaseURL);
        System.out.println("ReqURL"+requestUrl);
       // System.out.println("ReqURL"+object.get);

        if(isAuthRequired)
        {
             headers.set("Authorization","bearer "+token.getSmartziApiToken());
             HttpEntity<Object> entity = new HttpEntity<>(object,headers);
          //   String response = restTemplate.postForEntity(SmartziBaseURL + requestUrl, entity, String.class).getBody();
             result = restTemplate.exchange(requestUrl,  HttpMethod.POST, entity, String.class);
         //  System.out.println("resp"+response);
        }
        else {
             HttpEntity<Object> entity = new HttpEntity<>(object,headers);
             result = restTemplate.exchange(requestUrl, HttpMethod.POST, entity, String.class);
        }

        return result.getBody();
    }

   /* @Override
    public String getSmartziWeb(Object object, String requestUrl, boolean isAuthRequired) {

        if(isAuthRequired)
        {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization","Bearer "+token.getSmartziApiToken());
            HttpEntity<Object> entity = new HttpEntity<Object>(object, headers);
            ResponseEntity<String> result = restTemplate.exchange(requestUrl,  HttpMethod.POST, entity, String.class);
            result.getBody()
        }
        else
        {

        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth("admin", "admin");

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);

        builder.queryParam("CustomerID", "123");
        builder.queryParam("DSType", "SensorData");
        builder.queryParam("DomainID", "PowerPlants");
        builder.queryParam("ItemKey", "DPP1A2");
        builder.queryParam("Path", "DPP");
        builder.queryParam("Attribute", "*");

// String requestJson = "{}";
        String requestJson = "{}";

        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);

// String response= restTemplate.exchange(url, HttpMethod.POST,
// entity,String.class, map).getBody().toString();
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.POST,
                entity,
                String.class);
        System.out.println("responseEntity***" + builder.build().encode().toUri());

        return responseEntity.getBody();
    }
*/


}
