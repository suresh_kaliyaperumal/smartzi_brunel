package com.brunel.business.impl;

import com.brunel.business.RestCallManagement;
import com.brunel.business.TripManagement;
import com.brunel.constants.ApplicationConstant;
import com.brunel.constants.SupplierRideStatusRideStatus;
import com.brunel.dto.request.*;
import com.brunel.dto.response.*;
import com.brunel.entity.*;
import com.brunel.exception.Errors;
import com.brunel.repository.AdminRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TripManagementImpl implements TripManagement {

    @Autowired
    RestCallManagement restCallManagement;

    @Value("${smartzi.b2bdetail.appId}")
    private String b2bdetailFromAppId;

    @Autowired
    AdminRepository adminRepository;

    @Value("${smartzi.cancel.trip}")
    private String tripCancelUrl;

    @Value("${smartzi.trip.reservation.detail}")
    private String tripReservationUrl;

    @Value("${smartzi.b2b.booking.detail}")
    private String tripBookingDetailUrl;

  /*  @Autowired
    CommonValidator commonValidator;*/
/*
    @Value("${smartzi.v1.quote.id}")
    private String viewQuoteIdURL;*/
/*
    @Value("${smartzi.v1.address.detail}")
    private String quoteAddresURL;*/

    @Value("${smartzi.common.viewquote}")
    private String smartziOfferRequestURL;

    @Value("${smartzi.com.booking}")
    private String comBookingURL;

    @Value("${smartzi.com.paymentid}")
    private Long brunelPamentId;

   /* @Value("${smartzi.account.id}")
    private Long b2bAccountDetailId;*/



    @Override
    public BrunelViewQuoteResponse getInventoryQuote(BrunelBookingB1Request quoteCompleteDetail) {

        ObjectMapper objMap = new ObjectMapper();
        ViewQuoteIdResponse quoteIdResponse = null;
        Gson gson = new Gson();
        Gson gson1 = new Gson();
        JsonParser parser = new JsonParser();
        JsonObject jsonObj;
        B2BViewQuoteDetailEntity b2bViewQuoteDetailEntity = new B2BViewQuoteDetailEntity();
        BrunelViewQuoteResponse brunelViewQuoteResponse = new BrunelViewQuoteResponse();

        try {
         //  commonValidator.inventoryQuoteValidator(viewQuotesRequest);
            ViewQuoteId quoteResponse = new ViewQuoteId();
            B2BDetailFromAppIdRequest b2bdetailFromAppIdRequest = new B2BDetailFromAppIdRequest();

            b2bdetailFromAppIdRequest.setSupplierId(ApplicationConstant.SMARTZI_SUPPLIERID);
            String b2bdetailFromAppIdRes = restCallManagement.getSmartziWebService(b2bdetailFromAppIdRequest, b2bdetailFromAppId, true);
            jsonObj = (JsonObject) parser.parse(b2bdetailFromAppIdRes);
            B2BDetailFromAppResponse b2bDetailFromAppIdResponse = gson1.fromJson(jsonObj.get("data"), B2BDetailFromAppResponse.class);
            if (b2bDetailFromAppIdResponse.getB2bAccountDetailId() == null || b2bDetailFromAppIdResponse.getB2bAccountDetailId() == 0)
                throw new Errors(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ApplicationConstant.ACCOUNT_DETAIL_ID);
            System.out.println("UserID"+b2bDetailFromAppIdResponse.getUserId());
            quoteIdResponse = gson.fromJson(jsonObj, ViewQuoteIdResponse.class);
            if (quoteIdResponse.getCode() != null && quoteIdResponse.getMessage() != null)
                throw new Errors(quoteIdResponse.getCode(), quoteIdResponse.getMessage());
            System.out.println("ReturnId" + quoteIdResponse.getId());
            quoteResponse.setQuoteId(quoteIdResponse.getId());

            SmartziEngineQuoteRequest engineQuoteRequest = new SmartziEngineQuoteRequest();

            if(quoteCompleteDetail.getStops().getStop().size()>2)
            {
                throw new Errors(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Multiple Location Not Allowed");
            }

            engineQuoteRequest.setCurrentLocationLat(quoteCompleteDetail.getStops().getStop().get(0).getGridY());
            engineQuoteRequest.setCurrentLocationLong(quoteCompleteDetail.getStops().getStop().get(0).getGridX());
            engineQuoteRequest.setSourceAddress(quoteCompleteDetail.getStops().getStop().get(0).getAddress_name() + " " + quoteCompleteDetail.getStops().getStop().get(0).getAddress1()
                    + " " + quoteCompleteDetail.getStops().getStop().get(0).getAddress2()
                    + " " + quoteCompleteDetail.getStops().getStop().get(0).getAddress3() + " " + quoteCompleteDetail.getStops().getStop().get(0).getAddress4()
                    + " " + quoteCompleteDetail.getStops().getStop().get(0).getPostcode() + " " + quoteCompleteDetail.getStops().getStop().get(0).getCountry());
            engineQuoteRequest.setSourceLat(quoteCompleteDetail.getStops().getStop().get(0).getGridY());
            engineQuoteRequest.setSourceLong(quoteCompleteDetail.getStops().getStop().get(0).getGridX());

            engineQuoteRequest.setDestinationAddress(quoteCompleteDetail.getStops().getStop().get(1).getAddress_name() + " " + quoteCompleteDetail.getStops().getStop().get(1).getAddress1()
                    + " " + quoteCompleteDetail.getStops().getStop().get(1).getAddress2()
                    + " " + quoteCompleteDetail.getStops().getStop().get(1).getAddress3() + " " + quoteCompleteDetail.getStops().getStop().get(1).getAddress4()
                    + " " + quoteCompleteDetail.getStops().getStop().get(1).getPostcode() + " " + quoteCompleteDetail.getStops().getStop().get(1).getCountry());
            engineQuoteRequest.setDestinationLat(quoteCompleteDetail.getStops().getStop().get(1).getGridY());
            engineQuoteRequest.setDestinationLong(quoteCompleteDetail.getStops().getStop().get(1).getGridY());

            engineQuoteRequest.setCustomerId(0L);
            engineQuoteRequest.setGuestTripRequestId(0L);
            engineQuoteRequest.setPromoCode("");
            engineQuoteRequest.setBookingSource(1005);
            engineQuoteRequest.setBookingSelect(1206);
            engineQuoteRequest.setBookingSourceType(1108);
            engineQuoteRequest.setAppPlatformId(3);
            engineQuoteRequest.setB2BAccountDetailId(b2bDetailFromAppIdResponse.getB2bAccountDetailId());
            System.out.println("AccountDetailId"+b2bDetailFromAppIdResponse.getB2bAccountDetailId());
            engineQuoteRequest.setAgentDetailId(0L);
            engineQuoteRequest.setFromB2BPortal(true);
            engineQuoteRequest.setReferenceNo("");
            engineQuoteRequest.setDriverDetailIds(new Long[0]);
            engineQuoteRequest.setRoutePointsAry(new String[0]);
            engineQuoteRequest.setUserId(b2bDetailFromAppIdResponse.getUserId());
            engineQuoteRequest.setSkipFlightInfo(true);
            engineQuoteRequest.setVehicleTypeId(1L);
            engineQuoteRequest.setRoundTrip(false);

            List<QuoteRoundTripDetailRequest> tripDetailRequestList = new ArrayList<>();
            QuoteRoundTripDetailRequest tripDetailRequest = new QuoteRoundTripDetailRequest();
            tripDetailRequest.setExternalId("");
            Date date = null;
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            date = calendar.getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            tripDetailRequest.setTravelDateTime(quoteCompleteDetail.getPickup_dt());
            System.out.println("TomorrowDTandTime>>>>>>>>>>" + tripDetailRequest.getTravelDateTime());
            tripDetailRequest.setSourceTimeZone("");
            //System.out.println("TomorrowDTandTime>>>>>>>>>>" + quoteCompleteDetail.getOriginViewQuoteDetail().getTime_zone());

            tripDetailRequest.setFlightNumber(quoteCompleteDetail.getStops().getStop().get(0).getFlight().getFlight_no());

            SimpleDateFormat dtConvert = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            SimpleDateFormat sdt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

            Date travel_Dt = sdt.parse(quoteCompleteDetail.getStops().getStop().get(0).getFlight().getFlight_dt());
            String flight_dt = dtConvert.format(travel_Dt);

            tripDetailRequest.setFlightDatetime(flight_dt);
            if(quoteCompleteDetail.getGoods().getGood().get(0).getType()!=2)
                throw new Errors(3,"Parcel is not Allowed");
            tripDetailRequest.setPassangerCount((short)quoteCompleteDetail.getGoods().getGood().size());
            tripDetailRequest.setLauggageCount((short)quoteCompleteDetail.getGoods().getGood().size());
            tripDetailRequest.setVehicleClass((short)0);
            tripDetailRequestList.add(tripDetailRequest);
            engineQuoteRequest.setTripDetails(tripDetailRequestList);
            String smartziEngineQuotes = restCallManagement.getSmartziWebService(engineQuoteRequest, smartziOfferRequestURL, true);
            System.out.println("Returnengine quote"+smartziEngineQuotes);
            JsonObject jsonObj1 = (JsonObject) parser.parse(smartziEngineQuotes);
            SmartziEngineViewQuoteResponse engineViewQuoteResponse = gson.fromJson(jsonObj1.get("data"), SmartziEngineViewQuoteResponse.class);
            if (engineViewQuoteResponse.getOutBound().getStatusCode() != 0)
                throw new Errors(engineViewQuoteResponse.getOutBound().getStatusCode(), engineViewQuoteResponse.getOutBound().getMsg());
            b2bViewQuoteDetailEntity.setRequestId(quoteCompleteDetail.getHost_ref());
            b2bViewQuoteDetailEntity.setRequest(objMap.writeValueAsString(quoteCompleteDetail));
            b2bViewQuoteDetailEntity.setResponse(smartziEngineQuotes);
            b2bViewQuoteDetailEntity.setSupplierId(ApplicationConstant.SMARTZI_SUPPLIERID);
            ViewQuoteDynamoDBResponse viewQuoteDynamoDBResponse = adminRepository.insertViewQuoteDetails(b2bViewQuoteDetailEntity);
            brunelViewQuoteResponse.setId(viewQuoteDynamoDBResponse.getRequestId());
            List<QuoteResponse> quoteResponseList = new ArrayList<>();
          //  for (int i = 0; i < engineViewQuoteResponse.getOutBound().getRequestOffers().size(); i++) {

                String offerId = null;
                QuoteResponse quoteItems = new QuoteResponse();
                B2BRequestOfferEntity smartziEngReqOffersDynamoDb = new B2BRequestOfferEntity();
                System.out.println("TripRequestId>>>>>>>>>>>>>"+engineViewQuoteResponse.getOutBound().getTripRequestId());
                smartziEngReqOffersDynamoDb.setQuoteRequestId(viewQuoteDynamoDBResponse.getRequestId());
                smartziEngReqOffersDynamoDb.setTripRequestId(engineViewQuoteResponse.getOutBound().getTripRequestId());
                smartziEngReqOffersDynamoDb.setTripRequestDriverId(engineViewQuoteResponse.getOutBound().getRequestOffers().get(0).getDriverDetailId());
                smartziEngReqOffersDynamoDb.setTripRequestOfferId(engineViewQuoteResponse.getOutBound().getRequestOffers().get(0).getTripRequestOfferId());
                offerId = adminRepository.insertRequestOffer(smartziEngReqOffersDynamoDb);
                brunelViewQuoteResponse.setId(viewQuoteDynamoDBResponse.getRequestId());
                brunelViewQuoteResponse.setQuoteId(offerId);
                brunelViewQuoteResponse.setEstimatedFare(engineViewQuoteResponse.getOutBound().getRequestOffers().get(0).getEstimatedFare());
                brunelViewQuoteResponse.setTripRequestId(engineViewQuoteResponse.getOutBound().getTripRequestId());
                brunelViewQuoteResponse.setTripRequestOfferId(engineViewQuoteResponse.getOutBound().getRequestOffers().get(0).getTripRequestOfferId());
                brunelViewQuoteResponse.setDriverId(engineViewQuoteResponse.getOutBound().getRequestOffers().get(0).getDriverDetailId());
                brunelViewQuoteResponse.setEstimatedRideDurationSeconds(engineViewQuoteResponse.getOutBound().getEstimatedRideDurationSeconds());


                /* quoteItems.setQuoteId(offerId);
                quoteItems.setTripRequestOfferId(engineViewQuoteResponse.getOutBound().getRequestOffers().get(0).getTripRequestOfferId());
                quoteItems.setTripRequestId(engineViewQuoteResponse.getOutBound().getTripRequestId());*/
               /* quoteItems.setCurrency_code("GBP");
                quoteItems.setCategory_name(engineViewQuoteResponse.getOutBound().getRequestOffers().get(i).getDescription());
                quoteItems.setFleet_id(engineViewQuoteResponse.getOutBound().getRequestOffers().get(i).getVehicleTypeId().toString());
                quoteItems.setFleet_name(engineViewQuoteResponse.getOutBound().getRequestOffers().get(i).getVehicleTypeName());
                quoteItems.setHigh_price(engineViewQuoteResponse.getOutBound().getRequestOffers().get(i).getEstimatedFare() * 100);
                quoteItems.setLow_price(engineViewQuoteResponse.getOutBound().getRequestOffers().get(i).getEstimatedFare() * 100);
                quoteItems.setPhone_no("");
                quoteItems.setPick_up_type("DEFAULT");
                quoteItems.setQta_high_minutes(0);
                quoteItems.setQta_low_minutes(0);
               // quoteItems.setQuote_id(quoteCompleteDetail.getViewQuoteId() + ":" + offerId);
                quoteItems.setQuote_id(offerId);
                quoteItems.setSource("");
                quoteItems.setQuote_type("");
                quoteItems.setSupplier_logo_url("");
                quoteItems.setTerms_conditions_url("");
                quoteItems.setFleet_description("");
                quoteItems.setVehicle_attributes(new VehicleAttributes[0]);
                quoteItems.setVehicle_class("");
                quoteItemsList.add(quoteItems);*/
              //  quoteResponseList.add(quoteItems);
           // }
          //  karhooViewQuoteResponse.setQuote_items(quoteResponseList);

          } catch (JsonProcessingException | ParseException e) {
            b2bViewQuoteDetailEntity.setException(e.toString());
            adminRepository.insertViewQuoteDetails(b2bViewQuoteDetailEntity);
            e.printStackTrace();
        }

        return brunelViewQuoteResponse;



    }


    @Override
    public String engineCancelBooking(String id,CancelRequest cancelRequest) {
    SmartziCancelBooking smartziCancelBooking = new SmartziCancelBooking();
    CancelRideEntity cancelRideEntity = new CancelRideEntity();
    BrunelCancelResponse brunelCancelResponse = new BrunelCancelResponse();
    String engineResponse = null;
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
         ObjectMapper objMap = new ObjectMapper();

        try {
        RideDetailEntity rideDetailEntity = adminRepository.getRideDetail(id);
        smartziCancelBooking.setTripReservationId(rideDetailEntity.getReservationId());
        smartziCancelBooking.setTripCancelReasonId((long) -1);
        smartziCancelBooking.setCancelReason(cancelRequest.getReason());
        smartziCancelBooking.setTripReservationStatus(3);

        engineResponse  = restCallManagement.getSmartziWebService(smartziCancelBooking, tripCancelUrl, true);
            JsonObject jsonObj = (JsonObject) parser.parse(engineResponse);
            EngineCancelResponse cancelResponse = gson.fromJson(jsonObj.get("data"), EngineCancelResponse.class);
            if (jsonObj.get("status").getAsInt() != 1000)
                throw new Errors(HttpServletResponse.SC_NOT_FOUND, jsonObj.get("message").toString());

            if(cancelResponse.isResp()) {
                cancelRideEntity.setTripId(id);
                cancelRideEntity.setCancelReason(cancelRequest.getReason());
                cancelRideEntity.setRequest(objMap.writeValueAsString(cancelRequest));
                cancelRideEntity.setCancelReasonId(cancelResponse.getTripCancelReasonId());
                cancelRideEntity.setResponse(engineResponse);
                adminRepository.insertCancelRide(cancelRideEntity);
               /* karhooCancelResponse.setTrip_id(id);
                karhooCancelResponse.setStatus(ApplicationConstant.TRIP_CANCELLED);*/
            }
            else
            {
                throw new Errors(HttpServletResponse.SC_NOT_FOUND,cancelResponse.getMsg());
            }
        } catch (IOException e) {
            cancelRideEntity.setException(e.toString());
            cancelRideEntity.setTripId(id);
            adminRepository.insertCancelRide(cancelRideEntity);
            e.printStackTrace();
        }

        return "Booking has been successfully cancelled";
    }

    @Override
    public BrunelBookingResponse engineBooking(BrunelBookingB1Request bookingRequest) {
        ObjectMapper objMap = new ObjectMapper();
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonObject jsonObj;
        RideDetailEntity rideDetailEntity = new RideDetailEntity();
       // JsonNode actualObj = null;
        CreateRideEntity createRideEntity = new CreateRideEntity();
        RideDetailEntity rideDetailEntityInsert = new RideDetailEntity();
        BrunelBookingResponse brunelBookingResponse = new BrunelBookingResponse();
       //B2BDetailFromAppIdRequest b2bDetailFromAppIdRequest = new B2BDetailFromAppIdRequest();


        try {

            // format of the date
        SimpleDateFormat dtConvert = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        SimpleDateFormat sdt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

        Date travel_Dt = sdt.parse(bookingRequest.getPickup_dt());
        String dt = sdt.format(new Date());
        Date currDt = sdt.parse(dt);
            bookingRequest.setPickup_dt(dtConvert.format(travel_Dt));
        if(currDt.after(travel_Dt)) {
            throw new Errors(HttpServletResponse.SC_NOT_FOUND, ApplicationConstant.SCHEDULED_DATE_CHECK);
        }

            BrunelViewQuoteResponse brunelViewQuoteResponse = getInventoryQuote(bookingRequest);

            rideDetailEntity = adminRepository.getRideDetail(brunelViewQuoteResponse.getQuoteId());
            System.out.println("Check 2");

          if(rideDetailEntity.getReservationId() == null || rideDetailEntity.getReservationId() == 0 ) {
            System.out.println("Input>>>>>>>>>>>>>>>>>>" + brunelViewQuoteResponse.getQuoteId());
            B2BRequestOfferEntity reqOffersEntityFrmDynamoDb = adminRepository.getSmartziEngReqOffers(brunelViewQuoteResponse.getQuoteId());
            System.out.println("reqOffersEntityFrmDynamoDb.getOfferId()>>>>>>>>>>>>>" + reqOffersEntityFrmDynamoDb.getOfferId());

            if (reqOffersEntityFrmDynamoDb.getOfferId() != null && !reqOffersEntityFrmDynamoDb.getOfferId().isEmpty()) {
                TripBookingRideRequest tripBookingRideRequest = new TripBookingRideRequest();
                List<TripBookingRideRequest> bookRidesRequestLst = new ArrayList<>();

             /*   System.out.println("DriverId" + reqOffersEntityFrmDynamoDb.getTripRequestDriverId());
                tripBookingRideRequest.setCountryCode(bookingRequest.getPickup().getAddress().getCountryCode());
                tripBookingRideRequest.setDriverDetailId(reqOffersEntityFrmDynamoDb.getTripRequestDriverId());
                tripBookingRideRequest.setTripRequestId(reqOffersEntityFrmDynamoDb.getTripRequestId());
                tripBookingRideRequest.setTripRequestOfferId(reqOffersEntityFrmDynamoDb.getTripRequestOfferId());
                tripBookingRideRequest.setCustomerId(0L);
               // tripBookingRideRequest.setTravelDateTime(travelDateTime);
                tripBookingRideRequest.setSourceTimeZone("");
                tripBookingRideRequest.setFlightNumber(bookingRequest.getFlight().getFlightNo());
                tripBookingRideRequest.setFlightDatetime(bookingRequest.getFlight().getFlightDate());
                tripBookingRideRequest.setPassangerCount(bookingRequest.getConstraints().getPassengersNo());
                tripBookingRideRequest.setLauggageCount(bookingRequest.getConstraints().getSuitcasesNo());
                tripBookingRideRequest.setReferralCode("");
                tripBookingRideRequest.setAdminNotes("");
                tripBookingRideRequest.setReason("");
                tripBookingRideRequest.setAgentDetailId(0L);
                tripBookingRideRequest.setGuestTripRequestId(0L);
                tripBookingRideRequest.setPaymentMode((short) 1);
                tripBookingRideRequest.setPaymentAccountId(brunelPamentId);
                tripBookingRideRequest.setRemarks(bookingRequest.getPassengerNote());
                tripBookingRideRequest.setB2BMarkup(0.0);
                tripBookingRideRequest.setCustomFareApplied(false);
                tripBookingRideRequest.setCustomEstimatedFare(0.0);
                tripBookingRideRequest.setCustomDriverEstimatedFare(0.0);
                tripBookingRideRequest.setUserId(0L);
                tripBookingRideRequest.setTripReservationId(0L);
                tripBookingRideRequest.setBookingSourceId(1005);
                tripBookingRideRequest.setSelfDriverAccountName("");
                tripBookingRideRequest.setGuestBooking(true);
                tripBookingRideRequest.setFirstName(bookingRequest.getPassenger().getName());
                tripBookingRideRequest.setLastName("");
                //tripBookingRideRequest.setCountryId(countryDetailList.get(0).getPhoneCode());
                tripBookingRideRequest.setMobileNo(bookingRequest.getPassenger().getPhoneNumber());
                tripBookingRideRequest.setEmailId(bookingRequest.getPassenger().getEmail());
                tripBookingRideRequest.setStripePayToken("");
                tripBookingRideRequest.setRequiredCustomerSMSNotification(true);
                tripBookingRideRequest.setRequiredCustomerEmailNotification(true);
                tripBookingRideRequest.setRequiredDriverSMSNotification(true);
                tripBookingRideRequest.setSupplierId(ApplicationConstant.SMARTZI_SUPPLIERID);
                tripBookingRideRequest.setRequiredDriverEmailNotification(true);
                bookRidesRequestLst.add(tripBookingRideRequest);*/
                // TripBookingEngRespList bookRidesEngLstResObj = new TripBookingEngRespList();


                String comBookingResp = restCallManagement.getSmartziWebService(bookRidesRequestLst, comBookingURL, true);
                // actualObj = new ObjectMapper().readTree(response);
                System.out.println("Returnengine quote" + comBookingResp);
                JsonObject comBookingJson = (JsonObject) parser.parse(comBookingResp);
                List<EngineBookingResponse> engineBookingResponse = gson.fromJson(comBookingJson.get("data"), new TypeToken<List<EngineBookingResponse>>() {}.getType());
                if (comBookingJson.get("status").getAsInt() != 1000)
                    throw new Errors(HttpServletResponse.SC_NOT_FOUND, comBookingJson.get("message").toString());
                createRideEntity.setTripId(brunelViewQuoteResponse.getQuoteId());
                createRideEntity.setOfferId(brunelViewQuoteResponse.getTripRequestOfferId().toString());
                createRideEntity.setRequest(objMap.writeValueAsString(bookingRequest));
                createRideEntity.setResponse(objMap.writeValueAsString(engineBookingResponse.get(0)));
                createRideEntity.setSupplierId(ApplicationConstant.SMARTZI_SUPPLIERID);

                rideDetailEntityInsert.setTripId(brunelViewQuoteResponse.getQuoteId());
                rideDetailEntityInsert.setRequestId(engineBookingResponse.get(0).getTripRequestId());
                rideDetailEntityInsert.setReservationId(engineBookingResponse.get(0).getTripReservationId());
                rideDetailEntityInsert.setSupplierId(ApplicationConstant.SMARTZI_SUPPLIERID);
                adminRepository.insertCreateBooking(createRideEntity);
                adminRepository.insertRideDetails(rideDetailEntityInsert);

                brunelBookingResponse.setTripId(brunelViewQuoteResponse.getQuoteId());
                brunelBookingResponse.setEstimatedFare(engineBookingResponse.get(0).getEstimatedTripFare()*100);
                brunelBookingResponse.setBookingReference(engineBookingResponse.get(0).getBookingId());
                brunelBookingResponse.setEstimatedRideDurationSeconds(brunelViewQuoteResponse.getEstimatedRideDurationSeconds());
            } else {
                throw new Errors(HttpServletResponse.SC_BAD_REQUEST, ApplicationConstant.TRIP_NOT_AVAILABLE);
            }
        }
        else {
            throw new Errors(HttpServletResponse.SC_BAD_REQUEST, ApplicationConstant.TRIPID_ALREADY_EXISTS);
        }

        } catch (IOException | ParseException e) {
            rideDetailEntity.setException(e.toString());
            adminRepository.insertRideDetails(rideDetailEntityInsert);
            e.printStackTrace();
        }

            return brunelBookingResponse;
    }

    @Override
    public BrunelBookingStatus engineTripStatus(String tripId) {

        ObjectMapper objMap = new ObjectMapper();
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        TripStatusEntity tripStatusEntity = new TripStatusEntity();
        BrunelBookingStatus bookingStatus = new BrunelBookingStatus();

        try {
        RideDetailEntity rideDetailEntity = adminRepository.getRideDetail(tripId);
        GetTripReservDetailRequest getTripReservDetailRequest = new GetTripReservDetailRequest();
        getTripReservDetailRequest.setTripReservationId(rideDetailEntity.getReservationId());

        System.out.println("reservationId"+rideDetailEntity.getReservationId());
        String  smartziResponse = restCallManagement.getSmartziWebService(getTripReservDetailRequest, tripReservationUrl, true);
            JsonObject jsonObj = (JsonObject) parser.parse(smartziResponse);
            DriverTrackingEntity cancelResponse = gson.fromJson(jsonObj.get("data"), DriverTrackingEntity.class);
            if (jsonObj.get("status").getAsInt() != 1000)
                throw new Errors(HttpServletResponse.SC_NOT_FOUND, jsonObj.get("message").toString());
            bookingStatus.setRideId(tripId);
            bookingStatus.setStatus(getStatus(cancelResponse));
            DriverLocationResponse driverLocation = new DriverLocationResponse();
            driverLocation.setLatitude(cancelResponse.getDriversLocation().getLocation().getLatitude());
            driverLocation.setLongitude(cancelResponse.getDriversLocation().getLocation().getLongitude());
            bookingStatus.setDriverLocation(driverLocation);
             tripStatusEntity.setTripId(tripId);
            tripStatusEntity.setRideStatusRequest(tripId);
            tripStatusEntity.setRideStatusResponse(objMap.writeValueAsString(cancelResponse));
            adminRepository.insertTripStatusDetail(tripStatusEntity);

        } catch (IOException e) {
            tripStatusEntity.setException(e.toString());
            adminRepository.insertTripStatusDetail(tripStatusEntity);
            e.printStackTrace();
        }
        return bookingStatus;

    }

    @Override
    public RideDetailsResponse engineTripDetail(String id) {

        ObjectMapper objMap = new ObjectMapper();
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();

        RideDetailsResponse rideDetailsResponse = new RideDetailsResponse();
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        JsonNode actualObj = null;
        BookingDetailResponse b2bBookingEntity = new BookingDetailResponse();
        List<B2BBookingEntity> b2bBookingEntityLst = new ArrayList<>();
        OriginRequest pickup = new OriginRequest();
        DestinationRequest destination = new DestinationRequest();

        PointRequest pickUpPoint = new PointRequest();
        PointRequest destinationPoint = new PointRequest();

        AddressRequest pickupAddress = new AddressRequest();
        AddressRequest destinationAddress = new AddressRequest();
        RideDetailsRequest rideDetailsRequest = new RideDetailsRequest();

        FlightRequest flight = new FlightRequest();
        PickUpRequest pickupTime = new PickUpRequest();
        PassengerRequest passenger = new PassengerRequest();
        OperatorRequest operator = new OperatorRequest();
        VehicleType vehicleType = new VehicleType();
        ConstraintsRequest constraints = new ConstraintsRequest();
        AssignedDriver assignedDriver = new AssignedDriver();

        try {
           /* // Using rideid to get requestId
            SmartziEngReqOffers smartziEngReqOffers = offerrepository.getRideIdReqOffers(rideId);
            // Using requestId to get RideOfferRequest table data
            RideOfferRequest rideOfferReqFrmDynamoDb =
                    offerrepository.getRideOfferRequest(smartziEngReqOffers.getRequestId());*/

            RideDetailEntity rideDetailEntity = adminRepository.getRideDetail(id);
            if(rideDetailEntity == null) {
                throw new Errors(HttpServletResponse.SC_NOT_FOUND, ApplicationConstant.TRIP_NOT_AVAILABLE);
            }
            else {
                CreateRideEntity createRideEntity = adminRepository.getRideJsonDetail(id);
                InventoryBookingViewQuoteRequest tripBookingResponse = objMap.readValue(createRideEntity.getRequest(), InventoryBookingViewQuoteRequest.class);
                /*bookingDetailRequest.setTripReservationId(rideDetailEntity.getReservationId());
                String smartziResponse = restCallManagement.getSmartziWebService(bookingDetailRequest, tripBookingDetailUrl, true);*/
                rideDetailsRequest.setTripReservationId(rideDetailEntity.getReservationId());
                String smartziResponse = restCallManagement.getSmartziWebService(rideDetailsRequest, tripBookingDetailUrl, true);
                JsonObject jsonObj = (JsonObject) parser.parse(smartziResponse);
                List<BookingDetailResponse> bookingDetailResponse = gson.fromJson(jsonObj.get("data"), new TypeToken<List<BookingDetailResponse>>() {
                }.getType());
                if (jsonObj.get("status").getAsInt() != 1000)
                    throw new Errors(HttpServletResponse.SC_NOT_FOUND, jsonObj.get("message").toString());


                b2bBookingEntity = bookingDetailResponse.get(0);
                // pickUp
                pickUpPoint.setLatitude(tripBookingResponse.getPickup().getPoint().getLatitude());
                pickUpPoint.setLongitude(tripBookingResponse.getPickup().getPoint().getLatitude());
                pickup.setPoint(pickUpPoint);

               /* if (smartziEngReqOffers.getRoundTrip() == 1) {
                    pickupAddress.setHouseNumber(rideOfferReqFrmDynamoDb.getPickupHouseNumber());
                    pickupAddress.setStreet(rideOfferReqFrmDynamoDb.getPickupStreet());
                    pickupAddress.setDistrict(rideOfferReqFrmDynamoDb.getPickupDistrict());
                    pickupAddress.setCity(rideOfferReqFrmDynamoDb.getPickupCity());
                    pickupAddress.setState(rideOfferReqFrmDynamoDb.getPickupState());
                    pickupAddress.setCountryCode(rideOfferReqFrmDynamoDb.getPickupCountryCode());
                    pickupAddress.setCountry(rideOfferReqFrmDynamoDb.getPickupCountry());
                    pickupAddress.setPostalCode(rideOfferReqFrmDynamoDb.getPickupPostCode());
                    pickupAddress.setAirportCode(rideOfferReqFrmDynamoDb.getPickupAirportCode());
                    pickup.setAddress(pickupAddress);
                } else {*/
                    pickupAddress.setHouseNumber(tripBookingResponse.getPickup().getAddress().getHouseNumber());
                    pickupAddress.setStreet(tripBookingResponse.getPickup().getAddress().getStreet());
                    pickupAddress.setDistrict(tripBookingResponse.getPickup().getAddress().getDistrict());
                    pickupAddress.setCity(tripBookingResponse.getPickup().getAddress().getCity());
                    pickupAddress.setState(tripBookingResponse.getPickup().getAddress().getState());
                    pickupAddress.setCountryCode(tripBookingResponse.getPickup().getAddress().getCountryCode());
                    pickupAddress.setCountry(tripBookingResponse.getPickup().getAddress().getCountry());
                    pickupAddress.setPostalCode(tripBookingResponse.getPickup().getAddress().getPostalCode());
                    pickupAddress.setAirportCode(tripBookingResponse.getPickup().getAddress().getAirportCode());
                    pickup.setAddress(pickupAddress);
              //  }

                String flightDtTime = b2bBookingEntity.getFlightDtTime();
                if (flightDtTime != null && !flightDtTime.equals("")) {
                    String[] flightDtTimeArray = flightDtTime.split("\\s+");
                    flight.setFlightDate(flightDtTimeArray[0]);
                    flight.setFlightTime(flightDtTimeArray[1]);
                    flight.setFlightNo(b2bBookingEntity.getFlightNo());
                } else {
                    flight.setFlightDate("");
                    flight.setFlightTime("");
                    flight.setFlightNo(b2bBookingEntity.getFlightNo());
                }
                pickup.setFlight(flight);

                // Destination
                destinationPoint.setLatitude(b2bBookingEntity.getDestinationLat());
                destinationPoint.setLongitude(b2bBookingEntity.getDestinationLong());
                destination.setPoint(destinationPoint);

               /* if (smartziEngReqOffers.getRoundTrip() == 1) {
                    destinationAddress.setHouseNumber(rideOfferReqFrmDynamoDb.getDestinationHouseNumber());
                    destinationAddress.setStreet(rideOfferReqFrmDynamoDb.getDestinationStreet());
                    destinationAddress.setDistrict(rideOfferReqFrmDynamoDb.getDestinationDistrict());
                    destinationAddress.setCity(rideOfferReqFrmDynamoDb.getDestinationCity());
                    destinationAddress.setState(rideOfferReqFrmDynamoDb.getDestinationState());
                    destinationAddress.setCountryCode(rideOfferReqFrmDynamoDb.getDestinationCountryCode());
                    destinationAddress.setCountry(rideOfferReqFrmDynamoDb.getDestinationCountry());
                    destinationAddress.setPostalCode(rideOfferReqFrmDynamoDb.getDestinationPostCode());
                    destinationAddress.setAirportCode(rideOfferReqFrmDynamoDb.getDestinationAirportCode());
                    destination.setAddress(destinationAddress);
                    destination.setFlight(flight);
                } else {*/
                    destinationAddress.setHouseNumber(tripBookingResponse.getDestination().getAddress().getHouseNumber());
                    destinationAddress.setStreet(tripBookingResponse.getDestination().getAddress().getStreet());
                    destinationAddress.setDistrict(tripBookingResponse.getDestination().getAddress().getDistrict());
                    destinationAddress.setCity(tripBookingResponse.getDestination().getAddress().getCity());
                    destinationAddress.setState(tripBookingResponse.getDestination().getAddress().getState());
                    destinationAddress.setCountryCode(tripBookingResponse.getDestination().getAddress().getCountryCode());
                    destinationAddress.setCountry(tripBookingResponse.getDestination().getAddress().getCountry());
                    destinationAddress.setPostalCode(tripBookingResponse.getDestination().getAddress().getPostalCode());
                    destinationAddress.setAirportCode(tripBookingResponse.getDestination().getAddress().getAirportCode());
                    destination.setAddress(destinationAddress);
                    destination.setFlight(flight);
              //  }

                pickupTime.setSecondsSinceEpoch(b2bBookingEntity.getTravelDateTime());

                passenger.setReferenceNo(tripBookingResponse.getPassenger().getReferenceNo());
                passenger.setName(tripBookingResponse.getPassenger().getName());
                passenger.setPhoneNumber(tripBookingResponse.getPassenger().getPhoneNumber());
                passenger.setPhoneCode(b2bBookingEntity.getCustomerCountryCode());
                passenger.setEmail(tripBookingResponse.getPassenger().getEmail());

                operator.setReferenceNo(tripBookingResponse.getOperator().getReferenceNo());
                operator.setName(tripBookingResponse.getOperator().getName());
                operator.setPhoneNumber(tripBookingResponse.getOperator().getPhoneNumber());
                operator.setPhoneCode(b2bBookingEntity.getCustomerCountryCode());
                operator.setEmail(tripBookingResponse.getOperator().getEmail());

                vehicleType.setVehicleTypeId(b2bBookingEntity.getVehicleTypeId());
                vehicleType.setVehicleTypeName(b2bBookingEntity.getVehicleTypeName());
                vehicleType.setDescription(b2bBookingEntity.getDescription());
                vehicleType.setImageUrl(b2bBookingEntity.getImageUrl());
                vehicleType.setBookingEstimatedPrice(b2bBookingEntity.getEstimatedFare());

                constraints.setPassengersNo(tripBookingResponse.getConstraints().getPassengersNo());
                constraints.setSuitcasesNo(tripBookingResponse.getConstraints().getSuitcasesNo());

                assignedDriver.setDriverId(b2bBookingEntity.getDriverId());
                assignedDriver.setDriverCode(b2bBookingEntity.getDriverCode());
                assignedDriver.setDriverName(b2bBookingEntity.getDriverFirstName() + " " + b2bBookingEntity.getDriverLastName());
                assignedDriver.setEmailId(b2bBookingEntity.getDriverEmailId());
                assignedDriver.setPhoneNo(b2bBookingEntity.getDriverMobileNo());

                // response
                rideDetailsResponse.setPickup(pickup);
                rideDetailsResponse.setDestination(destination);
                rideDetailsResponse.setPickupTime(pickupTime);
                rideDetailsResponse.setPassenger(passenger);
                rideDetailsResponse.setOperator(operator);
                rideDetailsResponse.setVehicleType(vehicleType);
                rideDetailsResponse.setConstraints(constraints);
                rideDetailsResponse.setAssignedDriver(assignedDriver);
                rideDetailsResponse.setCouponCode("");//to do
                rideDetailsResponse.setPassengerNote(b2bBookingEntity.getAdminNotes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            throw new Errors(HttpServletResponse.SC_BAD_REQUEST, "Invalid request");
            }
        return rideDetailsResponse;
    }


       /*ObjectMapper objMap = new ObjectMapper();
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        BookingDetailRequest bookingDetailRequest = new BookingDetailRequest();
        KarhooTripDetail karhooTripDetailResponse = new KarhooTripDetail();
        try {
         RideDetailEntity rideDetailEntity = adminRepository.getRideDetail(id);
        if(rideDetailEntity == null) {
            throw new Errors(HttpServletResponse.SC_NOT_FOUND, ApplicationConstant.TRIP_NOT_AVAILABLE);
        }
        else {
            CreateRideEntity createRideEntity = adminRepository.getRideJsonDetail(id);
            BrunelTripBookingRequest tripBookingResponse = objMap.readValue(createRideEntity.getRequest(), BrunelTripBookingRequest.class);
            bookingDetailRequest.setTripReservationId(rideDetailEntity.getReservationId());
            String smartziResponse = restCallManagement.getSmartziWebService(bookingDetailRequest, tripBookingDetailUrl, true);
            JsonObject jsonObj = (JsonObject) parser.parse(smartziResponse);
            List<BookingDetailResponse> bookingDetailResponse = gson.fromJson(jsonObj.get("data"), new TypeToken<List<BookingDetailResponse>>() {
            }.getType());
            if (jsonObj.get("status").getAsInt() != 1000)
                throw new Errors(HttpServletResponse.SC_NOT_FOUND, jsonObj.get("message").toString());

            KarhooPassengers karhooPassengers[] = new KarhooPassengers[tripBookingResponse.getPassengers().length];
            KarhooLuggage luggage = new KarhooLuggage();
            KarhooOrigin origin = new KarhooOrigin();
            PositionResponse positionResponse = new PositionResponse();
            KarhooDestination  destination = new KarhooDestination();
            KarhooQuote quote = new KarhooQuote();
            KarhooBreakDown karhooBreakDown = new KarhooBreakDown();
            KarhooBreakDown breakDown[] = new KarhooBreakDown[1];
            KarhooVehicleAttribute vehicleAttribute = new KarhooVehicleAttribute();
            KarhooFare fare = new KarhooFare();
            KarhooFleetInfo fleetInfo = new KarhooFleetInfo();
            KarhooVehicle karhooVehicle = new KarhooVehicle();
            KarhooDriver driver = new KarhooDriver();
            KarhooMeetingPoint meetingPoint = new KarhooMeetingPoint();
            KarhooAgent karhooAgent = new KarhooAgent();
            KarhooCancelledBy cancelledBy = new KarhooCancelledBy();
            KarhooMeta karhooMeta = new KarhooMeta();

            karhooTripDetailResponse.setId(id);
            System.out.println("KArhoo ID 1");
            for(int i=0;i<tripBookingResponse.getPassengers().length;i++)
            {
                 KarhooPassengers passengersObj = new KarhooPassengers();
                 passengersObj.setFirst_name(tripBookingResponse.getPassengers()[i].getFirst_name());
                 passengersObj.setLast_name(tripBookingResponse.getPassengers()[i].getLast_name());
                 passengersObj.setPhone_number(tripBookingResponse.getPassengers()[i].getPhone_number());
                 passengersObj.setEmail("");
                 passengersObj.setLocale("");
                 karhooPassengers[i] = passengersObj;
            }
            System.out.println("KArhoo ID 2");
            karhooTripDetailResponse.setPassenger_details(karhooPassengers);
            luggage.setTotal((short)0);
            karhooTripDetailResponse.setLuggage(luggage);
            karhooTripDetailResponse.setState_details("");
          //  origin.setDisplay_address(tripBookingResponse.getOrigin().getAddress().getDisplay_address());
            origin.setPlace_id("");
            origin.setPoi_type("");
            positionResponse.setLatitude(bookingDetailResponse.get(0).getSourceLat());
            positionResponse.setLongitude(bookingDetailResponse.get(0).getSourceLong());
            origin.setPosition(positionResponse);
            origin.setTimezone("");
            karhooTripDetailResponse.setOrigin(origin);
          //  destination.setDisplay_address(tripBookingResponse.getDestination().getAddress().getDisplay_address());
            destination.setPlace_id("");
            destination.setPoi_type("");
            PositionResponse destPosition = new PositionResponse();
            destPosition.setLatitude(bookingDetailResponse.get(0).getDestinationLat());
            destPosition.setLongitude(bookingDetailResponse.get(0).getDestinationLong());
            destination.setPosition(destPosition);
            destination.setTimezone("");
            karhooTripDetailResponse.setDestination(destination);
            karhooTripDetailResponse.setDate_scheduled(tripBookingResponse.getDate_scheduled());
            karhooBreakDown.setDescription("");
            karhooBreakDown.setName("");
            karhooBreakDown.setValue(0.0);
            breakDown[0]=karhooBreakDown;
            quote.setBreakdown(breakDown);
            quote.setCurrency("");
            quote.setGratuity_percent(0);
            quote.setQta_high_minutes(0);
            quote.setQta_low_minutes(0);
            quote.setTotal((short)0);
            quote.setType("");
            quote.setVehicle_class("");
            vehicleAttribute.setChild_seat(false);
            vehicleAttribute.setElectric(false);
            vehicleAttribute.setHybrid(false);
            vehicleAttribute.setLuggage_capacity((short)1);
            vehicleAttribute.setPassenger_capacity((short)8);
            quote.setVehicle_attributes(vehicleAttribute);
            karhooTripDetailResponse.setQuote(quote);
            KarhooBreakDown karhooBreakDown1 = new KarhooBreakDown();
            KarhooBreakDown breakDown1[] = new KarhooBreakDown[1];
            System.out.println("TotalFare"+bookingDetailResponse.get(0).getTotalFare());
            karhooBreakDown1.setValue(bookingDetailResponse.get(0).getTotalFare()*100);
            karhooBreakDown1.setName("");
            karhooBreakDown1.setDescription("");
            breakDown1[0]=karhooBreakDown1;
            fare.setBreakdown(breakDown1);
            fare.setCurrency("");
            fare.setGratuity_percent(0);

            fare.setTotal((long) (bookingDetailResponse.get(0).getTripBaseFare()*100));
            karhooTripDetailResponse.setFare(fare);
            karhooTripDetailResponse.setExternal_trip_id("");
            karhooTripDetailResponse.setDisplay_trip_id("");
            fleetInfo.setDescription("");
            fleetInfo.setEmail("");
           /// fleetInfo.setFleet_id(tripBookingResponse.getExternal_info().getFleet_id());
            fleetInfo.setLogo_url("");
            fleetInfo.setName("");
            fleetInfo.setPhone_number("");
            fleetInfo.setTerms_conditions_url("");
            karhooTripDetailResponse.setFleet_info(fleetInfo);
            KarhooVehicleAttribute vehicleAttribute1 = new KarhooVehicleAttribute();
            vehicleAttribute1.setPassenger_capacity((short)8);
            vehicleAttribute1.setLuggage_capacity((short)0);
            vehicleAttribute1.setHybrid(false);
            vehicleAttribute1.setElectric(false);
            vehicleAttribute1.setChild_seat(false);
            karhooVehicle.setAttributes(vehicleAttribute1);
            karhooVehicle.setDescription("");
            karhooVehicle.setVehicle_class("");
            karhooVehicle.setVehicle_license_plate("");
            driver.setFirst_name(bookingDetailResponse.get(0).getDriverFirstName());
            driver.setLast_name(bookingDetailResponse.get(0).getDriverLastName());
            driver.setPhone_number(bookingDetailResponse.get(0).getDriverMobileNo());
            driver.setLicense_number("");
            driver.setPhoto_url("");
            karhooVehicle.setDriver(driver);
            karhooTripDetailResponse.setVehicle(karhooVehicle);
            karhooTripDetailResponse.setPartner_trip_id("");
            karhooTripDetailResponse.setComments("");
          //  karhooTripDetailResponse.setFlight_number(tripBookingResponse.getOrigin().getAirport().getFlight_number());
            karhooTripDetailResponse.setTrain_number(tripBookingResponse.getTrain_number());
            karhooTripDetailResponse.setDate_booked("");
            meetingPoint.setInstructions("");
            meetingPoint.setNote("");
            meetingPoint.setType("");
            PositionResponse positionResponse1 = new PositionResponse();
            positionResponse1.setLongitude(0.0);
            positionResponse1.setLongitude(0.0);
            meetingPoint.setPosition(positionResponse1);
            karhooTripDetailResponse.setMeeting_point(meetingPoint);
            karhooAgent.setOrganisation_id("");
            karhooAgent.setOrganisation_name("");
            karhooAgent.setUser_id("");
            karhooAgent.setUser_name("");
            karhooTripDetailResponse.setAgent(karhooAgent);
            karhooTripDetailResponse.setCost_center_reference("");
            cancelledBy.setEmail("");
            cancelledBy.setFirst_name("");
            cancelledBy.setLast_name("");
            cancelledBy.setId("");
            karhooTripDetailResponse.setCancelled_by(cancelledBy);
            karhooTripDetailResponse.setFollow_code("");
            karhooTripDetailResponse.setMeta(karhooMeta);
            karhooTripDetailResponse.setTrain_time("");
        }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return karhooTripDetailResponse;*/


    public String getStatus(DriverTrackingEntity driverTrackingEntity) {

        String statusCode = null;
        int tripStatus = 0;
        Optional<DriverTrackingEntity> tripReservationObj = Optional.ofNullable(driverTrackingEntity);
        if (tripReservationObj.isPresent()) {
            if (Optional.ofNullable(driverTrackingEntity.getTripStatus()).orElse(0) != 0) {
                tripStatus = driverTrackingEntity.getTripStatus();
                switch (tripStatus) {
                    case 1:
                        statusCode = SupplierRideStatusRideStatus.AT_PICKUP;
                        break;
                    case 2:
                        statusCode = SupplierRideStatusRideStatus.PASSENGER_ON_BOARD;
                        break;
                    case 3:
                        statusCode = SupplierRideStatusRideStatus.DRIVER_EN_ROUTE;
                        break;
                    case 4:
                        statusCode = SupplierRideStatusRideStatus.COMPLETED;
                        break;
                    case 5:
                        statusCode = SupplierRideStatusRideStatus.CANCELED;
                        break;
                    case 6:
                        statusCode = SupplierRideStatusRideStatus.CANCELED;
                        break;
                    default:
                        statusCode = SupplierRideStatusRideStatus.UNKNOWN;
                        break;
                }
            } else {
                switch (driverTrackingEntity.getTripReservationStatus()) {
                    case 1:
                        statusCode = SupplierRideStatusRideStatus.ACCEPTED;
                        break;
                    case 2:
                        statusCode = SupplierRideStatusRideStatus.DRIVER_ASSIGNED;
                        break;
                    case 3:
                        statusCode = SupplierRideStatusRideStatus.CANCELED;
                        break;
                    case 4:
                        statusCode = SupplierRideStatusRideStatus.REJECTED;
                        break;
                    case 5:
                        statusCode = SupplierRideStatusRideStatus.ACCEPTED;
                        break;
                    case 6:
                        statusCode = SupplierRideStatusRideStatus.ACCEPTED;
                        break;
                    case 7:
                        statusCode = SupplierRideStatusRideStatus.ACCEPTED;
                        break;
                    default:
                        statusCode = SupplierRideStatusRideStatus.UNKNOWN;
                        break;
                }
            }
        }

        return statusCode;
    }


}
