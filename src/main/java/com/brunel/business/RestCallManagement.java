package com.brunel.business;

public interface RestCallManagement {

    /**
     * Smartzi Rest call webservice
     * @param object
     * @param requestUrl
     * @param isAuthRequired
     * @return
     */

    String getSmartziWebService(Object object, String requestUrl, boolean isAuthRequired);

  //  String getSmartziWeb(Object object, String requestUrl, boolean isAuthRequired);

  }
