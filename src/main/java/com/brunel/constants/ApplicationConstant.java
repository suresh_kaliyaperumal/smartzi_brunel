package com.brunel.constants;

public class ApplicationConstant {

    /** The Constant SUCCESS_STATUS. */
    public static final String SUCCESS_STATUS = "1000";

    /** The Constant INTERNAL_SERVER_ERROR. */
    public static final String INTERNAL_SERVER_ERROR = "1036";

    /** The Constant AUDIT_LOGGER. */
    public static final String AUDIT_LOGGER = "AUDIT_LOGGER";

    /** The Constant START_TIME. */
    public static final String START_TIME = "00:00";

    /** The Constant END_TIME. */
    public static final String END_TIME = "23:59";

    /** The Constant ZERO. */
    public static final Integer ZERO = 0;

    /** The Constant MASKING_SYMBOL. */
    public static final String MASKING_SYMBOL = "X";

    /** The Constant NUMBER_OF_DIGITS_TO_DISPLAY. */
    public static final Integer NUMBER_OF_DIGITS_TO_DISPLAY = 4;

    public static final String SUCCESS = "success";

    public static final String CURRENCY = "GBP";

    public static final String TYPE = "FIXED";

    public static final String SMARTZI_SUPPLIERID = "BRUNEL0001";

    //public static final String KARHOO_ACCESS_ID = "karhoob2b01";

 //   public static final String CANCEL_REASON  = "Cancelled by Karhoo";

    public static final String ONDEMAND_NOT_AVAILABLE  = "Ondemand Not Available";

    public static final String ALREADY_CANCELLED  = "Already Cancelled";

    public static final String TRIPID_ALREADY_EXISTS  = "Trip id already exists";

    public static final String TRIP_NOT_AVAILABLE  = "Trip Not Available";

    public static final String TRIP_NOT_FOUND  = "Trip Not Found";

    public static final String SCHEDULED_DATE_CHECK  = "UTC Date Time not less than current date";

    public static final String TRIP_CANCELLED = "Trip Cancelled";

    public static final String ACCOUNT_DETAIL_ID = "Account detail Id not found";

    public static final String PLACE_ID = "Place Id Mandatory";










}
