package com.brunel.constants;

public class SupplierRideStatusRideStatus {

    public static final String UNKNOWN="FAILED";
    public static final String REJECTED="DRIVER_CANCELLED";
    public static final String ACCEPTED="CONFIRMED";
    public static final String DRIVER_ASSIGNED="DRIVER_ASSIGNED";
    public static final String DRIVER_EN_ROUTE="DRIVER_EN_ROUTE";
    public static final String PASSENGER_ON_BOARD="PASSENGER_ON_BOARD";
    public static final String AT_DROPOFF="AT_DROPOFF";
    public static final String COMPLETED="COMPLETED";
    public static final String CANCELED="CITYTRANSFER_CANCELLED";
    public static final String AT_PICKUP="AT_PICKUP";

}
