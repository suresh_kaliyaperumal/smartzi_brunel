package com.brunel.dto.request;

public class BrunelJobFlag {

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
