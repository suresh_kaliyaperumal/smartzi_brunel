package com.brunel.dto.request;

import java.util.List;

public class BrunelGoods {


private List<BrunelGood> good;

    public List<BrunelGood> getGood() {
        return good;
    }

    public void setGood(List<BrunelGood> good) {
        this.good = good;
    }
}
