package com.brunel.dto.request;

public class PickUpRequest {

   private String secondsSinceEpoch;

    public String getSecondsSinceEpoch() {
        return secondsSinceEpoch;
    }

    public void setSecondsSinceEpoch(String secondsSinceEpoch) {
        this.secondsSinceEpoch = secondsSinceEpoch;
    }
}
