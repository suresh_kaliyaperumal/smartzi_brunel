package com.brunel.dto.request;

public class BrunelBookingB1Request {

    private String host_ref;

    private String vendor_ref;


    private String account_ref;

    private String pickup_dt;

    private String tariff;

    private BrunelContact contact;

    private String wait_return;

    private String mop;

    private String special_instructions;

    private String despatch_instructions;

    private String job_notes;

    private BrunelStops stops;

    private BrunelGoods goods;

    private BrunelRefs refs;

    private Double fixed_price;

    private String currency_code;

    private String driver_callsign;

    private Double mileage;

    private BrunelExtras extras;

    private BrunelNotifications notifications;


    public String getHost_ref() {
        return host_ref;
    }

    public void setHost_ref(String host_ref) {
        this.host_ref = host_ref;
    }

    public String getVendor_ref() {
        return vendor_ref;
    }

    public void setVendor_ref(String vendor_ref) {
        this.vendor_ref = vendor_ref;
    }

    public String getAccount_ref() {
        return account_ref;
    }

    public void setAccount_ref(String account_ref) {
        this.account_ref = account_ref;
    }

    public String getPickup_dt() {
        return pickup_dt;
    }

    public void setPickup_dt(String pickup_dt) {
        this.pickup_dt = pickup_dt;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public BrunelContact getContact() {
        return contact;
    }

    public void setContact(BrunelContact contact) {
        this.contact = contact;
    }

    public String getWait_return() {
        return wait_return;
    }

    public void setWait_return(String wait_return) {
        this.wait_return = wait_return;
    }

    public String getMop() {
        return mop;
    }

    public void setMop(String mop) {
        this.mop = mop;
    }

    public String getSpecial_instructions() {
        return special_instructions;
    }

    public void setSpecial_instructions(String special_instructions) {
        this.special_instructions = special_instructions;
    }

    public String getDespatch_instructions() {
        return despatch_instructions;
    }

    public void setDespatch_instructions(String despatch_instructions) {
        this.despatch_instructions = despatch_instructions;
    }

    public String getJob_notes() {
        return job_notes;
    }

    public void setJob_notes(String job_notes) {
        this.job_notes = job_notes;
    }

    public BrunelStops getStops() {
        return stops;
    }

    public void setStops(BrunelStops stops) {
        this.stops = stops;
    }

    public BrunelGoods getGoods() {
        return goods;
    }

    public void setGoods(BrunelGoods goods) {
        this.goods = goods;
    }

    public BrunelRefs getRefs() {
        return refs;
    }

    public void setRefs(BrunelRefs refs) {
        this.refs = refs;
    }

    public Double getFixed_price() {
        return fixed_price;
    }

    public void setFixed_price(Double fixed_price) {
        this.fixed_price = fixed_price;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public String getDriver_callsign() {
        return driver_callsign;
    }

    public void setDriver_callsign(String driver_callsign) {
        this.driver_callsign = driver_callsign;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setMileage(Double mileage) {
        this.mileage = mileage;
    }

    public BrunelExtras getExtras() {
        return extras;
    }

    public void setExtras(BrunelExtras extras) {
        this.extras = extras;
    }

    public BrunelNotifications getNotifications() {
        return notifications;
    }

    public void setNotifications(BrunelNotifications notifications) {
        this.notifications = notifications;
    }
}
