package com.brunel.dto.request;

public class InventoryBookingViewQuoteRequest {

    private String supplierId;

    private String flightBookingReference;

    private OriginRequest pickup;

    private DestinationRequest destination;

    private FlightRequest flight;

    private PickUpRequest pickupTime;

    private PassengerRequest passenger;

    private Long vehicleTypeId;

    private Double estimatedFare;

    private  String currency;

    private OperatorRequest operator;

    private ConstraintsRequest constraints;

    private String travelDateTime;

    private String passengerNote;

    private String timeZone;


    public Long getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Long vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

        public String getTravelDateTime() {
        return travelDateTime;
    }

    public void setTravelDateTime(String travelDateTime) {
        this.travelDateTime = travelDateTime;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getFlightBookingReference() {
        return flightBookingReference;
    }

    public void setFlightBookingReference(String flightBookingReference) {
        this.flightBookingReference = flightBookingReference;
    }


    public DestinationRequest getDestination() {
        return destination;
    }

    public void setDestination(DestinationRequest destination) {
        this.destination = destination;
    }


    public PassengerRequest getPassenger() {
        return passenger;
    }

    public void setPassenger(PassengerRequest passenger) {
        this.passenger = passenger;
    }

    public OperatorRequest getOperator() {
        return operator;
    }

    public void setOperator(OperatorRequest operator) {
        this.operator = operator;
    }

    public ConstraintsRequest getConstraints() {
        return constraints;
    }

    public void setConstraints(ConstraintsRequest constraints) {
        this.constraints = constraints;
    }

    public String getPassengerNote() {
        return passengerNote;
    }

    public void setPassengerNote(String passengerNote) {
        this.passengerNote = passengerNote;
    }

    public OriginRequest getPickup() {
        return pickup;
    }

    public void setPickup(OriginRequest pickup) {
        this.pickup = pickup;
    }

    public FlightRequest getFlight() {
        return flight;
    }

    public void setFlight(FlightRequest flight) {
        this.flight = flight;
    }

    public PickUpRequest getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(PickUpRequest pickupTime) {
        this.pickupTime = pickupTime;
    }

    public Double getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(Double estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
