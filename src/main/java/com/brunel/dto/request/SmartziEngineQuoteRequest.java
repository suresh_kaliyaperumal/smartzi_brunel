package com.brunel.dto.request;

import java.util.List;

public class SmartziEngineQuoteRequest {

    private String requestId;
    private Long tripRequestId;
    private String externalId;
    private Double currentLocationLat;
    private Double currentLocationLong;
    private String sourceAddress;
    private Double sourceLat;
    private Double sourceLong;
    private String destinationAddress;
    private Double destinationLat;
    private Double destinationLong;
    private Long vehicleTypeId;
    private Long customerId;
    private Integer appPlatformId;
    private String[] routePointsAry;
    private Integer bookingSource;
    private Integer bookingSourceType;
    private Integer bookingSelect;
    private String referenceNo;
    private Long agentDetailId;
    private Long B2BAccountDetailId;
    private boolean fromB2BPortal;
    private Long guestTripRequestId;
    private Long userId;
    private String promoCode;
    private Long[] driverDetailIds;
    private boolean roundTrip;
    private boolean skipFlightInfo;
    private List<QuoteRoundTripDetailRequest> tripDetails;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Long getTripRequestId() {
        return tripRequestId;
    }

    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Double getCurrentLocationLat() {
        return currentLocationLat;
    }

    public void setCurrentLocationLat(Double currentLocationLat) {
        this.currentLocationLat = currentLocationLat;
    }

    public Double getCurrentLocationLong() {
        return currentLocationLong;
    }

    public void setCurrentLocationLong(Double currentLocationLong) {
        this.currentLocationLong = currentLocationLong;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public Double getSourceLat() {
        return sourceLat;
    }

    public void setSourceLat(Double sourceLat) {
        this.sourceLat = sourceLat;
    }

    public Double getSourceLong() {
        return sourceLong;
    }

    public void setSourceLong(Double sourceLong) {
        this.sourceLong = sourceLong;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public Double getDestinationLat() {
        return destinationLat;
    }

    public void setDestinationLat(Double destinationLat) {
        this.destinationLat = destinationLat;
    }

    public Double getDestinationLong() {
        return destinationLong;
    }

    public void setDestinationLong(Double destinationLong) {
        this.destinationLong = destinationLong;
    }

    public Long getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Long vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Integer getAppPlatformId() {
        return appPlatformId;
    }

    public void setAppPlatformId(Integer appPlatformId) {
        this.appPlatformId = appPlatformId;
    }

    public String[] getRoutePointsAry() {
        return routePointsAry;
    }

    public void setRoutePointsAry(String[] routePointsAry) {
        this.routePointsAry = routePointsAry;
    }

    public Integer getBookingSource() {
        return bookingSource;
    }

    public void setBookingSource(Integer bookingSource) {
        this.bookingSource = bookingSource;
    }

    public Integer getBookingSourceType() {
        return bookingSourceType;
    }

    public void setBookingSourceType(Integer bookingSourceType) {
        this.bookingSourceType = bookingSourceType;
    }

    public Integer getBookingSelect() {
        return bookingSelect;
    }

    public void setBookingSelect(Integer bookingSelect) {
        this.bookingSelect = bookingSelect;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public Long getAgentDetailId() {
        return agentDetailId;
    }

    public void setAgentDetailId(Long agentDetailId) {
        this.agentDetailId = agentDetailId;
    }

    public Long getB2BAccountDetailId() {
        return B2BAccountDetailId;
    }

    public void setB2BAccountDetailId(Long b2BAccountDetailId) {
        B2BAccountDetailId = b2BAccountDetailId;
    }

    public boolean isFromB2BPortal() {
        return fromB2BPortal;
    }

    public void setFromB2BPortal(boolean fromB2BPortal) {
        this.fromB2BPortal = fromB2BPortal;
    }

    public Long getGuestTripRequestId() {
        return guestTripRequestId;
    }

    public void setGuestTripRequestId(Long guestTripRequestId) {
        this.guestTripRequestId = guestTripRequestId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Long[] getDriverDetailIds() {
        return driverDetailIds;
    }

    public void setDriverDetailIds(Long[] driverDetailIds) {
        this.driverDetailIds = driverDetailIds;
    }

    public boolean isRoundTrip() {
        return roundTrip;
    }

    public void setRoundTrip(boolean roundTrip) {
        this.roundTrip = roundTrip;
    }

    public boolean isSkipFlightInfo() {
        return skipFlightInfo;
    }

    public void setSkipFlightInfo(boolean skipFlightInfo) {
        this.skipFlightInfo = skipFlightInfo;
    }

    public List<QuoteRoundTripDetailRequest> getTripDetails() {
        return tripDetails;
    }

    public void setTripDetails(List<QuoteRoundTripDetailRequest> tripDetails) {
        this.tripDetails = tripDetails;
    }
}
