package com.brunel.dto.request;

public class SmartziCancelBooking {

    private Long tripCancelReasonId;

    private Long userId;

    private String searchKey;

    private Integer offset;

    private Integer limit;

    private Integer reasonType;

    private String cancelReason;

    private Boolean applyonCustApp;

    private Boolean applyonDriverApp;

    private Boolean applyonCpanel;

    private Long tripRequestId;

    private int tripReservationStatus;

    private Long tripReservationId;


    public Long getTripCancelReasonId() {
        return tripCancelReasonId;
    }

    public void setTripCancelReasonId(Long tripCancelReasonId) {
        this.tripCancelReasonId = tripCancelReasonId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getReasonType() {
        return reasonType;
    }

    public void setReasonType(Integer reasonType) {
        this.reasonType = reasonType;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public Boolean getApplyonCustApp() {
        return applyonCustApp;
    }

    public void setApplyonCustApp(Boolean applyonCustApp) {
        this.applyonCustApp = applyonCustApp;
    }

    public Boolean getApplyonDriverApp() {
        return applyonDriverApp;
    }

    public void setApplyonDriverApp(Boolean applyonDriverApp) {
        this.applyonDriverApp = applyonDriverApp;
    }

    public Boolean getApplyonCpanel() {
        return applyonCpanel;
    }

    public void setApplyonCpanel(Boolean applyonCpanel) {
        this.applyonCpanel = applyonCpanel;
    }

    public Long getTripRequestId() {
        return tripRequestId;
    }

    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }

    public int getTripReservationStatus() {
        return tripReservationStatus;
    }

    public void setTripReservationStatus(int tripReservationStatus) {
        this.tripReservationStatus = tripReservationStatus;
    }

    public Long getTripReservationId() {
        return tripReservationId;
    }

    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }
}
