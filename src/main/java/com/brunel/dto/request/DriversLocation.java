package com.brunel.dto.request;

import com.brunel.dto.response.LocationResponse;
import org.springframework.beans.factory.parsing.Location;

public class DriversLocation {

    private LocationResponse location;


    public LocationResponse getLocation() {
        return location;
    }

    public void setLocation(LocationResponse location) {
        this.location = location;
    }
}
