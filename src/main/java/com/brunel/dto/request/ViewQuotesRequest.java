package com.brunel.dto.request;

public class ViewQuotesRequest {

    private String origin_place_id;

    private String destination_place_id;


    public String getOrigin_place_id() {
        return origin_place_id;
    }

    public void setOrigin_place_id(String origin_place_id) {
        this.origin_place_id = origin_place_id;
    }

    public String getDestination_place_id() {
        return destination_place_id;
    }

    public void setDestination_place_id(String destination_place_id) {
        this.destination_place_id = destination_place_id;
    }
}
