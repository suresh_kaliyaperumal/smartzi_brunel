package com.brunel.dto.request;

public class Notification {

    private String  type;

    private String event;

    private String name;

    private String contact_detail;

    private String  stop_num;

    private String   unit;

    private String earliest_dt_to_send;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact_detail() {
        return contact_detail;
    }

    public void setContact_detail(String contact_detail) {
        this.contact_detail = contact_detail;
    }

    public String getStop_num() {
        return stop_num;
    }

    public void setStop_num(String stop_num) {
        this.stop_num = stop_num;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getEarliest_dt_to_send() {
        return earliest_dt_to_send;
    }

    public void setEarliest_dt_to_send(String earliest_dt_to_send) {
        this.earliest_dt_to_send = earliest_dt_to_send;
    }
}
