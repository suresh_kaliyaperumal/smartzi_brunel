package com.brunel.dto.request;

import java.util.List;

public class BrunelStops {

    private List<Stop> stop;

    public List<Stop> getStop() {
        return stop;
    }

    public void setStop(List<Stop> stop) {
        this.stop = stop;
    }
}
