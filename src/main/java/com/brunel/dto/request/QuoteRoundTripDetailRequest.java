package com.brunel.dto.request;

public class QuoteRoundTripDetailRequest {

    private String externalId;
    private String travelDateTime;
    private String sourceTimeZone;
    private String flightNumber;
    private String flightDatetime;
    private Short passangerCount;
    private Short lauggageCount;
    private Short vehicleClass;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getTravelDateTime() {
        return travelDateTime;
    }

    public void setTravelDateTime(String travelDateTime) {
        this.travelDateTime = travelDateTime;
    }

    public String getSourceTimeZone() {
        return sourceTimeZone;
    }

    public void setSourceTimeZone(String sourceTimeZone) {
        this.sourceTimeZone = sourceTimeZone;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightDatetime() {
        return flightDatetime;
    }

    public void setFlightDatetime(String flightDatetime) {
        this.flightDatetime = flightDatetime;
    }

    public Short getPassangerCount() {
        return passangerCount;
    }

    public void setPassangerCount(Short passangerCount) {
        this.passangerCount = passangerCount;
    }

    public Short getLauggageCount() {
        return lauggageCount;
    }

    public void setLauggageCount(Short lauggageCount) {
        this.lauggageCount = lauggageCount;
    }

    public Short getVehicleClass() {
        return vehicleClass;
    }

    public void setVehicleClass(Short vehicleClass) {
        this.vehicleClass = vehicleClass;
    }
}
