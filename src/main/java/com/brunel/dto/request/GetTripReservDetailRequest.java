package com.brunel.dto.request;

public class GetTripReservDetailRequest {

    private Long tripRequestId;
    /**
     *
     */
    private Long tripReservationId;


    public Long getTripRequestId() {
        return tripRequestId;
    }

    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }

    public Long getTripReservationId() {
        return tripReservationId;
    }

    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }
}
