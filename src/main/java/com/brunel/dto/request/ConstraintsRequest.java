package com.brunel.dto.request;

public class ConstraintsRequest {

     private Short passengersNo;

     private Short suitcasesNo;

    public Short getPassengersNo() {
        return passengersNo;
    }

    public void setPassengersNo(Short passengersNo) {
        this.passengersNo = passengersNo;
    }

    public Short getSuitcasesNo() {
        return suitcasesNo;
    }

    public void setSuitcasesNo(Short suitcasesNo) {
        this.suitcasesNo = suitcasesNo;
    }
}
