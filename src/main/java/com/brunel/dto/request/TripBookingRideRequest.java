package com.brunel.dto.request;

public class TripBookingRideRequest {

    private Long tripRequestId;
    private Long tripRequestOfferId;
    private Long customerId;
    private String travelDateTime;
    private String sourceTimeZone;
    private String flightNumber;
    private String flightDatetime;
    private Short passangerCount;
    private Short lauggageCount;
    private String referralCode;
    private String adminNotes;
    private String reason;
    private Long agentDetailId;
    private Long guestTripRequestId;
    private Short paymentMode;
    private Long paymentAccountId;
    private String remarks;
    private Double b2BMarkup;
    private boolean customFareApplied;
    private Double customEstimatedFare;
    private Double customDriverEstimatedFare;
    private Long userId;
    private Long tripReservationId;
    private Integer bookingSourceId;
    private String selfDriverAccountName;
    private boolean isGuestBooking;
    private String firstName;
    private String lastName;
    private int countryId;
    private String mobileNo;
    private String emailId;
    private String stripePayToken;
    private boolean requiredCustomerSMSNotification;
    private boolean requiredDriverSMSNotification;
    private boolean requiredCustomerEmailNotification;
    private boolean requiredDriverEmailNotification;
    private Long driverDetailId;
    private String supplierId;
    private String countryCode;


    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public Long getTripRequestId() {
        return tripRequestId;
    }

    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }

    public Long getTripRequestOfferId() {
        return tripRequestOfferId;
    }

    public void setTripRequestOfferId(Long tripRequestOfferId) {
        this.tripRequestOfferId = tripRequestOfferId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getTravelDateTime() {
        return travelDateTime;
    }

    public void setTravelDateTime(String travelDateTime) {
        this.travelDateTime = travelDateTime;
    }

    public String getSourceTimeZone() {
        return sourceTimeZone;
    }

    public void setSourceTimeZone(String sourceTimeZone) {
        this.sourceTimeZone = sourceTimeZone;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightDatetime() {
        return flightDatetime;
    }

    public void setFlightDatetime(String flightDatetime) {
        this.flightDatetime = flightDatetime;
    }

    public Short getPassangerCount() {
        return passangerCount;
    }

    public void setPassangerCount(Short passangerCount) {
        this.passangerCount = passangerCount;
    }

    public Short getLauggageCount() {
        return lauggageCount;
    }

    public void setLauggageCount(Short lauggageCount) {
        this.lauggageCount = lauggageCount;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getAdminNotes() {
        return adminNotes;
    }

    public void setAdminNotes(String adminNotes) {
        this.adminNotes = adminNotes;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getAgentDetailId() {
        return agentDetailId;
    }

    public void setAgentDetailId(Long agentDetailId) {
        this.agentDetailId = agentDetailId;
    }

    public Long getGuestTripRequestId() {
        return guestTripRequestId;
    }

    public void setGuestTripRequestId(Long guestTripRequestId) {
        this.guestTripRequestId = guestTripRequestId;
    }

    public Short getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(Short paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Long getPaymentAccountId() {
        return paymentAccountId;
    }

    public void setPaymentAccountId(Long paymentAccountId) {
        this.paymentAccountId = paymentAccountId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Double getB2BMarkup() {
        return b2BMarkup;
    }

    public void setB2BMarkup(Double b2BMarkup) {
        this.b2BMarkup = b2BMarkup;
    }

    public boolean isCustomFareApplied() {
        return customFareApplied;
    }

    public void setCustomFareApplied(boolean customFareApplied) {
        this.customFareApplied = customFareApplied;
    }

    public Double getCustomEstimatedFare() {
        return customEstimatedFare;
    }

    public void setCustomEstimatedFare(Double customEstimatedFare) {
        this.customEstimatedFare = customEstimatedFare;
    }

    public Double getCustomDriverEstimatedFare() {
        return customDriverEstimatedFare;
    }

    public void setCustomDriverEstimatedFare(Double customDriverEstimatedFare) {
        this.customDriverEstimatedFare = customDriverEstimatedFare;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTripReservationId() {
        return tripReservationId;
    }

    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }

    public Integer getBookingSourceId() {
        return bookingSourceId;
    }

    public void setBookingSourceId(Integer bookingSourceId) {
        this.bookingSourceId = bookingSourceId;
    }

    public String getSelfDriverAccountName() {
        return selfDriverAccountName;
    }

    public void setSelfDriverAccountName(String selfDriverAccountName) {
        this.selfDriverAccountName = selfDriverAccountName;
    }

    public boolean isGuestBooking() {
        return isGuestBooking;
    }

    public void setGuestBooking(boolean guestBooking) {
        isGuestBooking = guestBooking;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getStripePayToken() {
        return stripePayToken;
    }

    public void setStripePayToken(String stripePayToken) {
        this.stripePayToken = stripePayToken;
    }

    public boolean isRequiredCustomerSMSNotification() {
        return requiredCustomerSMSNotification;
    }

    public void setRequiredCustomerSMSNotification(boolean requiredCustomerSMSNotification) {
        this.requiredCustomerSMSNotification = requiredCustomerSMSNotification;
    }

    public boolean isRequiredDriverSMSNotification() {
        return requiredDriverSMSNotification;
    }

    public void setRequiredDriverSMSNotification(boolean requiredDriverSMSNotification) {
        this.requiredDriverSMSNotification = requiredDriverSMSNotification;
    }

    public boolean isRequiredCustomerEmailNotification() {
        return requiredCustomerEmailNotification;
    }

    public void setRequiredCustomerEmailNotification(boolean requiredCustomerEmailNotification) {
        this.requiredCustomerEmailNotification = requiredCustomerEmailNotification;
    }

    public boolean isRequiredDriverEmailNotification() {
        return requiredDriverEmailNotification;
    }

    public void setRequiredDriverEmailNotification(boolean requiredDriverEmailNotification) {
        this.requiredDriverEmailNotification = requiredDriverEmailNotification;
    }

    public Long getDriverDetailId() {
        return driverDetailId;
    }

    public void setDriverDetailId(Long driverDetailId) {
        this.driverDetailId = driverDetailId;
    }
}
