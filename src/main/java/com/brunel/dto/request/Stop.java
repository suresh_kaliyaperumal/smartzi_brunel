package com.brunel.dto.request;

public class Stop {

    private Short drop_type;

    private Short drop_order;

    private String address_name;

    private String address1;

    private String address2;

    private String address3;

    private String address4;

    private String postcode;

    private String country;

    private Double gridY;

    private Double gridX;

    private String notes;

    private BrunelFlight flight;


    public BrunelFlight getFlight() {
        return flight;
    }

    public void setFlight(BrunelFlight flight) {
        this.flight = flight;
    }

    public Short getDrop_type() {
        return drop_type;
    }

    public void setDrop_type(Short drop_type) {
        this.drop_type = drop_type;
    }

    public Short getDrop_order() {
        return drop_order;
    }

    public void setDrop_order(Short drop_order) {
        this.drop_order = drop_order;
    }

    public String getAddress_name() {
        return address_name;
    }

    public void setAddress_name(String address_name) {
        this.address_name = address_name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getGridY() {
        return gridY;
    }

    public void setGridY(Double gridY) {
        this.gridY = gridY;
    }

    public Double getGridX() {
        return gridX;
    }

    public void setGridX(Double gridX) {
        this.gridX = gridX;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "Stop{" +
                "drop_type=" + drop_type +
                ", drop_order=" + drop_order +
                ", address_name='" + address_name + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", address3='" + address3 + '\'' +
                ", address4='" + address4 + '\'' +
                ", postcode='" + postcode + '\'' +
                ", country='" + country + '\'' +
                ", gridY=" + gridY +
                ", gridX=" + gridX +
                ", notes='" + notes + '\'' +
                '}';
    }
}
