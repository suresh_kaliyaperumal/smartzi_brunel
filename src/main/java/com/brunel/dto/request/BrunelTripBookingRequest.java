package com.brunel.dto.request;

public class BrunelTripBookingRequest {

    private String trip_id;

  //  private String partner_trip_id;

    private String date_scheduled;

    private String date_scheduled_utc;

  /*  private External_Info external_info;

    private KarhooTripOrigin origin;

    private KarhooTripDestination destination;*/

    private String comment;

    private Short passenger_count;

 //   private Passengers[] passengers;

   // private Quote quote;

    private String train_number;


    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getDate_scheduled() {
        return date_scheduled;
    }

    public void setDate_scheduled(String date_scheduled) {
        this.date_scheduled = date_scheduled;
    }

    public String getDate_scheduled_utc() {
        return date_scheduled_utc;
    }

    public void setDate_scheduled_utc(String date_scheduled_utc) {
        this.date_scheduled_utc = date_scheduled_utc;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Short getPassenger_count() {
        return passenger_count;
    }

    public void setPassenger_count(Short passenger_count) {
        this.passenger_count = passenger_count;
    }

  /*  public Passengers[] getPassengers() {
        return passengers;
    }

    public void setPassengers(Passengers[] passengers) {
        this.passengers = passengers;
    }
*/


    public String getTrain_number() {
        return train_number;
    }

    public void setTrain_number(String train_number) {
        this.train_number = train_number;
    }
}
