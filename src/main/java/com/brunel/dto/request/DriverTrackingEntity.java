package com.brunel.dto.request;

public class DriverTrackingEntity {

    private Long tripReservationId;
    private int tripReservationStatus;
    private int tripStatus;
    private DriversLocation driversLocation;

    public Long getTripReservationId() {
        return tripReservationId;
    }

    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }

    public int getTripReservationStatus() {
        return tripReservationStatus;
    }

    public void setTripReservationStatus(int tripReservationStatus) {
        this.tripReservationStatus = tripReservationStatus;
    }

    public int getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(int tripStatus) {
        this.tripStatus = tripStatus;
    }

    public DriversLocation getDriversLocation() {
        return driversLocation;
    }

    public void setDriversLocation(DriversLocation driversLocation) {
        this.driversLocation = driversLocation;
    }
}
