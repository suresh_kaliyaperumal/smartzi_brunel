package com.brunel.dto.request;

public class BrunelContact {

    private String contact_name;

    private String conact_phone;

    private String contact_fax;

    private String contact_email;

    private String contact_mobile;

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getConact_phone() {
        return conact_phone;
    }

    public void setConact_phone(String conact_phone) {
        this.conact_phone = conact_phone;
    }

    public String getContact_fax() {
        return contact_fax;
    }

    public void setContact_fax(String contact_fax) {
        this.contact_fax = contact_fax;
    }

    public String getContact_email() {
        return contact_email;
    }

    public void setContact_email(String contact_email) {
        this.contact_email = contact_email;
    }

    public String getContact_mobile() {
        return contact_mobile;
    }

    public void setContact_mobile(String contact_mobile) {
        this.contact_mobile = contact_mobile;
    }
}
