package com.brunel.dto.request;

public class BrunelFlight {

   private String  flight_no;

   private String from_to_city;

    private String flight_dt;

    private Short flight_terminal;

    private String flight_holdoff;

    public String getFlight_no() {
        return flight_no;
    }

    public void setFlight_no(String flight_no) {
        this.flight_no = flight_no;
    }

    public String getFrom_to_city() {
        return from_to_city;
    }

    public void setFrom_to_city(String from_to_city) {
        this.from_to_city = from_to_city;
    }

    public String getFlight_dt() {
        return flight_dt;
    }

    public void setFlight_dt(String flight_dt) {
        this.flight_dt = flight_dt;
    }

    public Short getFlight_terminal() {
        return flight_terminal;
    }

    public void setFlight_terminal(Short flight_terminal) {
        this.flight_terminal = flight_terminal;
    }

    public String getFlight_holdoff() {
        return flight_holdoff;
    }

    public void setFlight_holdoff(String flight_holdoff) {
        this.flight_holdoff = flight_holdoff;
    }
}
