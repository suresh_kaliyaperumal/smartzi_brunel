package com.brunel.dto.request;

import java.util.List;

public class BrunelNotifications {

    private List<Notification> notification;

    public List<Notification> getNotification() {
        return notification;
    }

    public void setNotification(List<Notification> notification) {
        this.notification = notification;
    }
}
