package com.brunel.dto.request;

public class BrunelRefs {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
