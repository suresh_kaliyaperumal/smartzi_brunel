package com.brunel.dto.request;

public class BrunelGood {

    private Short type;

    private String description;

    private Short quantity;

    private String phone;

    private String mobile;

    private String email;

    private Short  unit_weight;

    private Short unit_volume;

    private Short depth;

    private Short width;

    private Short  height;


    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Short getQuantity() {
        return quantity;
    }

    public void setQuantity(Short quantity) {
        this.quantity = quantity;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Short getUnit_weight() {
        return unit_weight;
    }

    public void setUnit_weight(Short unit_weight) {
        this.unit_weight = unit_weight;
    }

    public Short getUnit_volume() {
        return unit_volume;
    }

    public void setUnit_volume(Short unit_volume) {
        this.unit_volume = unit_volume;
    }

    public Short getDepth() {
        return depth;
    }

    public void setDepth(Short depth) {
        this.depth = depth;
    }

    public Short getWidth() {
        return width;
    }

    public void setWidth(Short width) {
        this.width = width;
    }

    public Short getHeight() {
        return height;
    }

    public void setHeight(Short height) {
        this.height = height;
    }
}
