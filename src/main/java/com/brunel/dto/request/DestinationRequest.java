package com.brunel.dto.request;

public class DestinationRequest {

    private PointRequest point;

    private AddressRequest address;

    private FlightRequest flight;


    public PointRequest getPoint() {
        return point;
    }

    public void setPoint(PointRequest point) {
        this.point = point;
    }

    public AddressRequest getAddress() {
        return address;
    }

    public void setAddress(AddressRequest address) {
        this.address = address;
    }

    public FlightRequest getFlight() {
        return flight;
    }

    public void setFlight(FlightRequest flight) {
        this.flight = flight;
    }
}
