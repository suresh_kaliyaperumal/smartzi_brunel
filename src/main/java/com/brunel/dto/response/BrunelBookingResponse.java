package com.brunel.dto.response;

public class BrunelBookingResponse {

    private String tripId;

    private String bookingReference;

    private Double estimatedFare;

    private String estimatedRideDurationSeconds;


    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getBookingReference() {
        return bookingReference;
    }

    public void setBookingReference(String bookingReference) {
        this.bookingReference = bookingReference;
    }

    public Double getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(Double estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public String getEstimatedRideDurationSeconds() {
        return estimatedRideDurationSeconds;
    }

    public void setEstimatedRideDurationSeconds(String estimatedRideDurationSeconds) {
        this.estimatedRideDurationSeconds = estimatedRideDurationSeconds;
    }
}
