package com.brunel.dto.response;

public class RideDetailsRequest {

    private Long tripReservationId;

    public Long getTripReservationId() {
        return tripReservationId;
    }

    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }
}
