package com.brunel.dto.response;

import java.util.List;

public class SmartziBound {


    private String estimatedRideDurationSeconds;
    private Long tripRequestId;
    private Boolean onDemand;
    private String referenceNo;
    private Double distance;
    private String estimatedTime;
    private List<RequestOffers> requestOffers;
    private String msg;
    private boolean res;
    private Integer statusCode;
    private boolean isAirportTrip;
    private boolean isAirportPickup;

    public String getEstimatedRideDurationSeconds() {
        return estimatedRideDurationSeconds;
    }

    public void setEstimatedRideDurationSeconds(String estimatedRideDurationSeconds) {
        this.estimatedRideDurationSeconds = estimatedRideDurationSeconds;
    }

    public Long getTripRequestId() {
        return tripRequestId;
    }

    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }

    public Boolean getOnDemand() {
        return onDemand;
    }

    public void setOnDemand(Boolean onDemand) {
        this.onDemand = onDemand;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public List<RequestOffers> getRequestOffers() {
        return requestOffers;
    }

    public void setRequestOffers(List<RequestOffers> requestOffers) {
        this.requestOffers = requestOffers;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isRes() {
        return res;
    }

    public void setRes(boolean res) {
        this.res = res;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isAirportTrip() {
        return isAirportTrip;
    }

    public void setAirportTrip(boolean airportTrip) {
        isAirportTrip = airportTrip;
    }

    public boolean isAirportPickup() {
        return isAirportPickup;
    }

    public void setAirportPickup(boolean airportPickup) {
        isAirportPickup = airportPickup;
    }
}
