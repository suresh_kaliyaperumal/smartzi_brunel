package com.brunel.dto.response;

public class TripStatusResponse {

    private String trip_id;
    private String status_code;
    private String status_information;
  //  private String account_id;




    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus_information() {
        return status_information;
    }

    public void setStatus_information(String status_information) {
        this.status_information = status_information;
    }
}
