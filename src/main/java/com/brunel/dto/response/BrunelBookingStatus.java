package com.brunel.dto.response;

public class BrunelBookingStatus {

    private String rideId;

    private String status;

    private DriverLocationResponse driverLocation;

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DriverLocationResponse getDriverLocation() {
        return driverLocation;
    }

    public void setDriverLocation(DriverLocationResponse driverLocation) {
        this.driverLocation = driverLocation;
    }
}
