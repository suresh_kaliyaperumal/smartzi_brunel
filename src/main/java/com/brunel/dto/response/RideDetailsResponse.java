package com.brunel.dto.response;


import com.brunel.dto.request.*;

public class RideDetailsResponse {

    private OriginRequest pickup;
    private DestinationRequest destination;
    private PickUpRequest pickupTime;
    private PassengerRequest passenger;
    private OperatorRequest operator;
    private VehicleType vehicleType;
    private ConstraintsRequest constraints;
//    private PreferredDriver preferred_driver;
    private AssignedDriver assignedDriver;
    private String couponCode;
    private String passengerNote;

    public OriginRequest getPickup() {
        return pickup;
    }

    public void setPickup(OriginRequest pickup) {
        this.pickup = pickup;
    }

    public DestinationRequest getDestination() {
        return destination;
    }

    public void setDestination(DestinationRequest destination) {
        this.destination = destination;
    }

    public PickUpRequest getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(PickUpRequest pickupTime) {
        this.pickupTime = pickupTime;
    }

    public PassengerRequest getPassenger() {
        return passenger;
    }

    public void setPassenger(PassengerRequest passenger) {
        this.passenger = passenger;
    }

    public OperatorRequest getOperator() {
        return operator;
    }

    public void setOperator(OperatorRequest operator) {
        this.operator = operator;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public ConstraintsRequest getConstraints() {
        return constraints;
    }

    public void setConstraints(ConstraintsRequest constraints) {
        this.constraints = constraints;
    }

    public AssignedDriver getAssignedDriver() {
        return assignedDriver;
    }

    public void setAssignedDriver(AssignedDriver assignedDriver) {
        this.assignedDriver = assignedDriver;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getPassengerNote() {
        return passengerNote;
    }

    public void setPassengerNote(String passengerNote) {
        this.passengerNote = passengerNote;
    }
}
