package com.brunel.dto.response;

public class ViewQuoteDynamoDBResponse {

    private String requestId;


    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }


}
