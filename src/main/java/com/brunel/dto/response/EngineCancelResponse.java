package com.brunel.dto.response;

public class EngineCancelResponse {

    private String msg;
    private boolean resp;
    private Long tripCancelReasonId;
    private String cancelReason;
    private Boolean boolApplyOnCustomerApp;
    private Boolean boolApplyOnCPanel;
    private Boolean boolApplyOnDriverApp;
    private String applyOnCustomerApp;
    private String applyOnCPanel;
    private String applyOnDriverApp;
    private Long count;
    private Long tripReservationId;
    private Long tripReservationCreatedBy;
    private Long tripRequestCreatedBy;
    private String referralCode;
    private String bookingId;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isResp() {
        return resp;
    }

    public void setResp(boolean resp) {
        this.resp = resp;
    }

    public Long getTripCancelReasonId() {
        return tripCancelReasonId;
    }

    public void setTripCancelReasonId(Long tripCancelReasonId) {
        this.tripCancelReasonId = tripCancelReasonId;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public Boolean getBoolApplyOnCustomerApp() {
        return boolApplyOnCustomerApp;
    }

    public void setBoolApplyOnCustomerApp(Boolean boolApplyOnCustomerApp) {
        this.boolApplyOnCustomerApp = boolApplyOnCustomerApp;
    }

    public Boolean getBoolApplyOnCPanel() {
        return boolApplyOnCPanel;
    }

    public void setBoolApplyOnCPanel(Boolean boolApplyOnCPanel) {
        this.boolApplyOnCPanel = boolApplyOnCPanel;
    }

    public Boolean getBoolApplyOnDriverApp() {
        return boolApplyOnDriverApp;
    }

    public void setBoolApplyOnDriverApp(Boolean boolApplyOnDriverApp) {
        this.boolApplyOnDriverApp = boolApplyOnDriverApp;
    }

    public String getApplyOnCustomerApp() {
        return applyOnCustomerApp;
    }

    public void setApplyOnCustomerApp(String applyOnCustomerApp) {
        this.applyOnCustomerApp = applyOnCustomerApp;
    }

    public String getApplyOnCPanel() {
        return applyOnCPanel;
    }

    public void setApplyOnCPanel(String applyOnCPanel) {
        this.applyOnCPanel = applyOnCPanel;
    }

    public String getApplyOnDriverApp() {
        return applyOnDriverApp;
    }

    public void setApplyOnDriverApp(String applyOnDriverApp) {
        this.applyOnDriverApp = applyOnDriverApp;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getTripReservationId() {
        return tripReservationId;
    }

    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }

    public Long getTripReservationCreatedBy() {
        return tripReservationCreatedBy;
    }

    public void setTripReservationCreatedBy(Long tripReservationCreatedBy) {
        this.tripReservationCreatedBy = tripReservationCreatedBy;
    }

    public Long getTripRequestCreatedBy() {
        return tripRequestCreatedBy;
    }

    public void setTripRequestCreatedBy(Long tripRequestCreatedBy) {
        this.tripRequestCreatedBy = tripRequestCreatedBy;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }
}
