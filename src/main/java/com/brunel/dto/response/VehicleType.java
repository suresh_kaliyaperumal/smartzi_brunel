package com.brunel.dto.response;

public class VehicleType {
	
	private int vehicleTypeId;
    private String vehicleTypeName;
    private String description;
    private String imageUrl;
	private double bookingEstimatedPrice;
	private Long tripRequestDriverId;
	private String offerId;

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public int getVehicleTypeId() {
		return vehicleTypeId;
	}

	public void setVehicleTypeId(int vehicleTypeId) {
		this.vehicleTypeId = vehicleTypeId;
	}

	public String getVehicleTypeName() {
		return vehicleTypeName;
	}
	public void setVehicleTypeName(String vehicleTypeName) {
		this.vehicleTypeName = vehicleTypeName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public double getBookingEstimatedPrice() {
		return bookingEstimatedPrice;
	}

	public void setBookingEstimatedPrice(double bookingEstimatedPrice) {
		this.bookingEstimatedPrice = bookingEstimatedPrice;
	}

	public Long getTripRequestDriverId() {
		return tripRequestDriverId;
	}

	public void setTripRequestDriverId(Long tripRequestDriverId) {
		this.tripRequestDriverId = tripRequestDriverId;
	}
}
