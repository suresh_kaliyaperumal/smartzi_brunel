package com.brunel.dto.response;

public class QuoteResponse {

    private String quoteId;

    private Long tripRequestId;

    private Long tripRequestOfferId;

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public Long getTripRequestId() {
        return tripRequestId;
    }

    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }

    public Long getTripRequestOfferId() {
        return tripRequestOfferId;
    }

    public void setTripRequestOfferId(Long tripRequestOfferId) {
        this.tripRequestOfferId = tripRequestOfferId;
    }
}
