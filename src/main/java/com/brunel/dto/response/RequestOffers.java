package com.brunel.dto.response;

public class RequestOffers {

    private Long tripRequestOfferId;
    private Integer driverEta;
    private Short passengerCount;
    private Short luggageCount;
    private Long driverDetailId;
    private Double ratePerMile;
    private Double promoDiscount;
    private Double b2BMarkup;
    private Double estimatedFare;
    private Double driverEstimatedFare;
    private Long vehicleTypeId;
    private String vehicleTypeName;
    private String imageUrl;
    private String description;

    public Long getTripRequestOfferId() {
        return tripRequestOfferId;
    }

    public void setTripRequestOfferId(Long tripRequestOfferId) {
        this.tripRequestOfferId = tripRequestOfferId;
    }

    public Integer getDriverEta() {
        return driverEta;
    }

    public void setDriverEta(Integer driverEta) {
        this.driverEta = driverEta;
    }

    public Short getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(Short passengerCount) {
        this.passengerCount = passengerCount;
    }

    public Short getLuggageCount() {
        return luggageCount;
    }

    public void setLuggageCount(Short luggageCount) {
        this.luggageCount = luggageCount;
    }

    public Long getDriverDetailId() {
        return driverDetailId;
    }

    public void setDriverDetailId(Long driverDetailId) {
        this.driverDetailId = driverDetailId;
    }

    public Double getRatePerMile() {
        return ratePerMile;
    }

    public void setRatePerMile(Double ratePerMile) {
        this.ratePerMile = ratePerMile;
    }

    public Double getPromoDiscount() {
        return promoDiscount;
    }

    public void setPromoDiscount(Double promoDiscount) {
        this.promoDiscount = promoDiscount;
    }

    public Double getB2BMarkup() {
        return b2BMarkup;
    }

    public void setB2BMarkup(Double b2BMarkup) {
        this.b2BMarkup = b2BMarkup;
    }

    public Double getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(Double estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public Double getDriverEstimatedFare() {
        return driverEstimatedFare;
    }

    public void setDriverEstimatedFare(Double driverEstimatedFare) {
        this.driverEstimatedFare = driverEstimatedFare;
    }

    public Long getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Long vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
