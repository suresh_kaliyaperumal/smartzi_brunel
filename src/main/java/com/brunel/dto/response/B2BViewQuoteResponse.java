package com.brunel.dto.response;

public class B2BViewQuoteResponse {

    private SmartziBound inbound;

    private OutBoundB2B outBound;

    public SmartziBound getInbound() {
        return inbound;
    }

    public void setInbound(SmartziBound inbound) {
        this.inbound = inbound;
    }

    public OutBoundB2B getOutBound() {
        return outBound;
    }

    public void setOutBound(OutBoundB2B outBound) {
        this.outBound = outBound;
    }
}
