package com.brunel.dto.response;

public class SmartziEngineViewQuoteResponse {

    private SmartziBound inbound;

    private SmartziBound outBound;

    public SmartziBound getInbound() {
        return inbound;
    }

    public void setInbound(SmartziBound inbound) {
        this.inbound = inbound;
    }

    public SmartziBound getOutBound() {
        return outBound;
    }

    public void setOutBound(SmartziBound outBound) {
        this.outBound = outBound;
    }
}
