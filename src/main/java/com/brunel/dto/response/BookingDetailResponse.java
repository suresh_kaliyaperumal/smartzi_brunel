package com.brunel.dto.response;

import java.sql.Timestamp;

public class BookingDetailResponse {

    private Long tripRequestId;
    private Long tripReservationId;
    private Long tripId;
    private Long tripPaymentId;
    private String bookingId;
    private String sourceAddress;
    private String destinationAddress;
    private String travelDateTime;
    private String uTCTravelDateTime;
    private String sourceTimeZone;
    private Short tripReservationStatus;
    private Short tripStatus;
    private String paymentMode;
    private Long driverId;
    private String driverName;
    private String driverMobileNo;
    private Long customerId;
    private String customerName;
    private String customerMobileNo;
    private Double estimatedFare;
    private String referenceNo;
    private Timestamp uTCTravelDateTimeDt;
    private String vehicleType;
    private Boolean airportTrip;
    private Boolean driverAutomatedTrip;
    private String adminNotes;
    private Double totalFare;
    private String agentCode;
    private Long count;

    private Double distance;

    private String estimatedTime;

    private Double standardDiscount;

    private Double tripBaseFare;

    private String customerCountryCode;

    private Long b2bAgentDetailId;


    private Double sourceLat;

    private Double sourceLong;

    private Double destinationLat;

    private Double destinationLong;

    private String driverFirstName;

    private String driverLastName;

    private String driverEmailId;

    private String customerFirstName;

    private String customerLastName;

    private String customerEmailId;

    private String agentFirstName;

    private String agentLastName;

    private String flightNo;

    private String flightDtTime;

    private String remarks;

    private Double finalTripFare;

    private Double promoDiscount;

    private Double parkingCharge;

    private Short tripResrvationType;

    private String utcTravelEndDateTime;

    private int eta;

    private Long userId;

    private int VehicleTypeId;

    private String VehicleTypeName;
    private String Description;
    private String ImageUrl;

    private String driverCode;


    private int sourceAirportDetailId;

    private int destinationAirportDetailId;


    private String cancelledUser;

    private String cancelReason;

    private String cancelledOn;


    public String getCancelledUser() {
        return cancelledUser;
    }

    public void setCancelledUser(String cancelledUser) {
        this.cancelledUser = cancelledUser;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getCancelledOn() {
        return cancelledOn;
    }

    public void setCancelledOn(String cancelledOn) {
        this.cancelledOn = cancelledOn;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public Double getStandardDiscount() {
        return standardDiscount;
    }

    public void setStandardDiscount(Double standardDiscount) {
        this.standardDiscount = standardDiscount;
    }

    public Double getTripBaseFare() {
        return tripBaseFare;
    }

    public void setTripBaseFare(Double tripBaseFare) {
        this.tripBaseFare = tripBaseFare;
    }

    public String getCustomerCountryCode() {
        return customerCountryCode;
    }

    public void setCustomerCountryCode(String customerCountryCode) {
        this.customerCountryCode = customerCountryCode;
    }

    public Long getB2bAgentDetailId() {
        return b2bAgentDetailId;
    }

    public void setB2bAgentDetailId(Long b2bAgentDetailId) {
        this.b2bAgentDetailId = b2bAgentDetailId;
    }

    public Double getSourceLat() {
        return sourceLat;
    }

    public void setSourceLat(Double sourceLat) {
        this.sourceLat = sourceLat;
    }

    public Double getSourceLong() {
        return sourceLong;
    }

    public void setSourceLong(Double sourceLong) {
        this.sourceLong = sourceLong;
    }

    public Double getDestinationLat() {
        return destinationLat;
    }

    public void setDestinationLat(Double destinationLat) {
        this.destinationLat = destinationLat;
    }

    public Double getDestinationLong() {
        return destinationLong;
    }

    public void setDestinationLong(Double destinationLong) {
        this.destinationLong = destinationLong;
    }

    public String getDriverFirstName() {
        return driverFirstName;
    }

    public void setDriverFirstName(String driverFirstName) {
        this.driverFirstName = driverFirstName;
    }

    public String getDriverLastName() {
        return driverLastName;
    }

    public void setDriverLastName(String driverLastName) {
        this.driverLastName = driverLastName;
    }

    public String getDriverEmailId() {
        return driverEmailId;
    }

    public void setDriverEmailId(String driverEmailId) {
        this.driverEmailId = driverEmailId;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCustomerEmailId() {
        return customerEmailId;
    }

    public void setCustomerEmailId(String customerEmailId) {
        this.customerEmailId = customerEmailId;
    }

    public String getAgentFirstName() {
        return agentFirstName;
    }

    public void setAgentFirstName(String agentFirstName) {
        this.agentFirstName = agentFirstName;
    }

    public String getAgentLastName() {
        return agentLastName;
    }

    public void setAgentLastName(String agentLastName) {
        this.agentLastName = agentLastName;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getFlightDtTime() {
        return flightDtTime;
    }

    public void setFlightDtTime(String flightDtTime) {
        this.flightDtTime = flightDtTime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Double getFinalTripFare() {
        return finalTripFare;
    }

    public void setFinalTripFare(Double finalTripFare) {
        this.finalTripFare = finalTripFare;
    }

    public Double getPromoDiscount() {
        return promoDiscount;
    }

    public void setPromoDiscount(Double promoDiscount) {
        this.promoDiscount = promoDiscount;
    }

    public Double getParkingCharge() {
        return parkingCharge;
    }

    public void setParkingCharge(Double parkingCharge) {
        this.parkingCharge = parkingCharge;
    }

    public Short getTripResrvationType() {
        return tripResrvationType;
    }

    public void setTripResrvationType(Short tripResrvationType) {
        this.tripResrvationType = tripResrvationType;
    }

    public String getUtcTravelEndDateTime() {
        return utcTravelEndDateTime;
    }

    public void setUtcTravelEndDateTime(String utcTravelEndDateTime) {
        this.utcTravelEndDateTime = utcTravelEndDateTime;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getVehicleTypeId() {
        return VehicleTypeId;
    }

    public void setVehicleTypeId(int vehicleTypeId) {
        VehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return VehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        VehicleTypeName = vehicleTypeName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getDriverCode() {
        return driverCode;
    }

    public void setDriverCode(String driverCode) {
        this.driverCode = driverCode;
    }

    public int getSourceAirportDetailId() {
        return sourceAirportDetailId;
    }

    public void setSourceAirportDetailId(int sourceAirportDetailId) {
        this.sourceAirportDetailId = sourceAirportDetailId;
    }

    public int getDestinationAirportDetailId() {
        return destinationAirportDetailId;
    }

    public void setDestinationAirportDetailId(int destinationAirportDetailId) {
        this.destinationAirportDetailId = destinationAirportDetailId;
    }

    public Long getTripRequestId() {
        return tripRequestId;
    }

    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }

    public Long getTripReservationId() {
        return tripReservationId;
    }

    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }

    public Long getTripId() {
        return tripId;
    }

    public void setTripId(Long tripId) {
        this.tripId = tripId;
    }

    public Long getTripPaymentId() {
        return tripPaymentId;
    }

    public void setTripPaymentId(Long tripPaymentId) {
        this.tripPaymentId = tripPaymentId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getTravelDateTime() {
        return travelDateTime;
    }

    public void setTravelDateTime(String travelDateTime) {
        this.travelDateTime = travelDateTime;
    }

    public String getuTCTravelDateTime() {
        return uTCTravelDateTime;
    }

    public void setuTCTravelDateTime(String uTCTravelDateTime) {
        this.uTCTravelDateTime = uTCTravelDateTime;
    }

    public String getSourceTimeZone() {
        return sourceTimeZone;
    }

    public void setSourceTimeZone(String sourceTimeZone) {
        this.sourceTimeZone = sourceTimeZone;
    }

    public Short getTripReservationStatus() {
        return tripReservationStatus;
    }

    public void setTripReservationStatus(Short tripReservationStatus) {
        this.tripReservationStatus = tripReservationStatus;
    }

    public Short getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(Short tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverMobileNo() {
        return driverMobileNo;
    }

    public void setDriverMobileNo(String driverMobileNo) {
        this.driverMobileNo = driverMobileNo;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public Double getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(Double estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public Timestamp getuTCTravelDateTimeDt() {
        return uTCTravelDateTimeDt;
    }

    public void setuTCTravelDateTimeDt(Timestamp uTCTravelDateTimeDt) {
        this.uTCTravelDateTimeDt = uTCTravelDateTimeDt;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Boolean getAirportTrip() {
        return airportTrip;
    }

    public void setAirportTrip(Boolean airportTrip) {
        this.airportTrip = airportTrip;
    }

    public Boolean getDriverAutomatedTrip() {
        return driverAutomatedTrip;
    }

    public void setDriverAutomatedTrip(Boolean driverAutomatedTrip) {
        this.driverAutomatedTrip = driverAutomatedTrip;
    }

    public String getAdminNotes() {
        return adminNotes;
    }

    public void setAdminNotes(String adminNotes) {
        this.adminNotes = adminNotes;
    }

    public Double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(Double totalFare) {
        this.totalFare = totalFare;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
