package com.brunel.dto.response;

import java.util.List;

public class OutBoundB2B {
    private String estimatedRideDurationSeconds;

    private List<VehicleType> vehicleType;

    private String msg;

    private boolean res;

    private Integer statusCode;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isRes() {
        return res;
    }

    public void setRes(boolean res) {
        this.res = res;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getEstimatedRideDurationSeconds() {
        return estimatedRideDurationSeconds;
    }

    public void setEstimatedRideDurationSeconds(String estimatedRideDurationSeconds) {
        this.estimatedRideDurationSeconds = estimatedRideDurationSeconds;
    }

    public List<VehicleType> getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(List<VehicleType> vehicleType) {
        this.vehicleType = vehicleType;
    }
}
