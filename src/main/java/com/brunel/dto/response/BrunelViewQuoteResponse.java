package com.brunel.dto.response;

import java.util.List;

public class BrunelViewQuoteResponse {

    private String id;

    private String estimatedRideDurationSeconds;

    private Double estimatedFare;

    private String quoteId;

    private Long tripRequestId;

    private Long tripRequestOfferId;

    private Long driverId;


    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEstimatedRideDurationSeconds() {
        return estimatedRideDurationSeconds;
    }

    public void setEstimatedRideDurationSeconds(String estimatedRideDurationSeconds) {
        this.estimatedRideDurationSeconds = estimatedRideDurationSeconds;
    }

    public Double getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(Double estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public Long getTripRequestId() {
        return tripRequestId;
    }

    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }

    public Long getTripRequestOfferId() {
        return tripRequestOfferId;
    }

    public void setTripRequestOfferId(Long tripRequestOfferId) {
        this.tripRequestOfferId = tripRequestOfferId;
    }

    @Override
    public String toString() {
        return "BrunelViewQuoteResponse{" +
                "id='" + id + '\'' +
                ", estimatedRideDurationSeconds='" + estimatedRideDurationSeconds + '\'' +
                ", estimatedFare=" + estimatedFare +
                ", quoteId='" + quoteId + '\'' +
                ", tripRequestId=" + tripRequestId +
                ", tripRequestOfferId=" + tripRequestOfferId +
                ", driverId=" + driverId +
                '}';
    }
}
