package com.brunel.dto.response;

public class B2BDetailFromAppResponse {

    private String appId;
    private String secretKey;
    private String supplierId;
    private Long b2bAccountDetailId;
    private Long userId;
    private String msg;
    private boolean res;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public Long getB2bAccountDetailId() {
        return b2bAccountDetailId;
    }

    public void setB2bAccountDetailId(Long b2bAccountDetailId) {
        this.b2bAccountDetailId = b2bAccountDetailId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isRes() {
        return res;
    }

    public void setRes(boolean res) {
        this.res = res;
    }
}
