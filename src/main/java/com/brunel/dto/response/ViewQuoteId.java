package com.brunel.dto.response;

public class ViewQuoteId {

    private String quoteId;

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }
}
