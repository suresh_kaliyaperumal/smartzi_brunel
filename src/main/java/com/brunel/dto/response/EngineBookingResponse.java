package com.brunel.dto.response;

public class EngineBookingResponse {

    private Long tripRequestId;
    private Long tripReservationId;
    private Long tripRequestOfferId;
    private String referenceNo;
    private String bookingId;
    private boolean res;
    private String msg;
    private Integer statusCode;
    private String paymentMode;
    private short tripType;
    private double estimatedTripFare;
    private int bookingType;
    private int bookingMode;




    public Long getTripRequestId() {
        return tripRequestId;
    }

    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }

    public Long getTripReservationId() {
        return tripReservationId;
    }

    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }

    public Long getTripRequestOfferId() {
        return tripRequestOfferId;
    }

    public void setTripRequestOfferId(Long tripRequestOfferId) {
        this.tripRequestOfferId = tripRequestOfferId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public boolean isRes() {
        return res;
    }

    public void setRes(boolean res) {
        this.res = res;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public short getTripType() {
        return tripType;
    }

    public void setTripType(short tripType) {
        this.tripType = tripType;
    }

    public double getEstimatedTripFare() {
        return estimatedTripFare;
    }

    public void setEstimatedTripFare(double estimatedTripFare) {
        this.estimatedTripFare = estimatedTripFare;
    }

    public int getBookingType() {
        return bookingType;
    }

    public void setBookingType(int bookingType) {
        this.bookingType = bookingType;
    }

    public int getBookingMode() {
        return bookingMode;
    }

    public void setBookingMode(int bookingMode) {
        this.bookingMode = bookingMode;
    }
}
