package com.brunel.dto.response;

import java.util.List;

public class TripBookingEngRespList {

    private List<TripBookingResponse> bookRides;

    public List<TripBookingResponse> getBookRides() {
        return bookRides;
    }

    public void setBookRides(List<TripBookingResponse> bookRides) {
        this.bookRides = bookRides;
    }
}
