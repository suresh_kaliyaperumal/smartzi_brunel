package com.brunel.exception;

public class Errors extends RuntimeException {

    private  String message;

    private  int code;

    private String karhooCode;

    public Errors( int code, String msg) {
        this.message =  msg;
        this.code = code;
    }

    public Errors( String karhooCode, String msg) {
        this.message =  msg;
        this.karhooCode = karhooCode;
    }


    public String getKarhooCode() {
        return karhooCode;
    }

    public void setKarhooCode(String karhooCode) {
        this.karhooCode = karhooCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
