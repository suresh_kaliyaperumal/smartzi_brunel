package com.brunel.exception;


public class ExceptionResponse {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private String message;

    private int status;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
