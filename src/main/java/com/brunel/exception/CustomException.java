package com.brunel.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;


@ControllerAdvice
public class CustomException
{
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public  @ResponseBody
    ExceptionResponse handleUserNotFoundException(WebRequest request, HttpRequestMethodNotSupportedException e) {
        ExceptionResponse exceptionResponse=new ExceptionResponse();
        exceptionResponse.setMessage("some problem description");
        exceptionResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        /*ErrorDetails error=new ErrorDetails();
        error.setName("Internal Server Error");
        error.setMessage("No message available");
        ErrorDetails[] errorDetails =new ErrorDetails[1];
        errorDetails[0]=error;*/
        //  exceptionResponse.setError(errorDetails);
        return exceptionResponse;
    }

    @ExceptionHandler(NullPointerException.class)
    public  @ResponseBody ExceptionResponse internalServerError(WebRequest request) {
        ExceptionResponse exceptionResponse=new ExceptionResponse();
        exceptionResponse.setMessage("Not Available");
        exceptionResponse.setStatus(HttpStatus.NOT_FOUND.value());
      /*  ErrorDetails error=new ErrorDetails();
        error.setName("Internal Server Error one ");
        error.setMessage("yes message available");
        ErrorDetails[] errorDetails =new ErrorDetails[1];
        errorDetails[0]=error;
        exceptionResponse.setError(errorDetails);*/
        return exceptionResponse;
    }

    @ExceptionHandler(Errors.class)
    public  @ResponseBody ExceptionResponse customException (WebRequest request, Errors errors) {
        ExceptionResponse exceptionResponse=new ExceptionResponse();
        exceptionResponse.setMessage(errors.getMessage());
        exceptionResponse.setStatus(errors.getCode());
      /*  ErrorDetails error=new ErrorDetails();
        error.setName("Internal Server Error one ");
        error.setMessage("yes message available");
        ErrorDetails[] errorDetails =new ErrorDetails[1];
        errorDetails[0]=error;
        exceptionResponse.setError(errorDetails);*/
        return exceptionResponse;
    }




}
