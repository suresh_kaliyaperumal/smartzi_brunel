package com.brunel.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.brunel.dto.response.ViewQuoteDynamoDBResponse;
import com.brunel.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class AdminRepository {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Value("${dynamoDb.TableName.prefix}")
    private String prefixTableName;


    public void insertAccessToken(TokenEntity tokenEntity) {

        String tableName = "test-com-token";
        dynamoDBMapper.save(tokenEntity, new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build());
    }

    public String getSmartziAccessToken(String supplierId) {
        TokenEntity partitionKey = new TokenEntity();
        partitionKey.setSupplierId(supplierId);

        DynamoDBQueryExpression<TokenEntity> queryExpression =
                new DynamoDBQueryExpression<TokenEntity>()
                        .withHashKeyValues(partitionKey);
        String tableName = prefixTableName+"token";
        return dynamoDBMapper.query(TokenEntity.class, queryExpression,
                new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build()).get(0).getAccessToken();


       /* TokenEntity tokenEntity = dynamoDBMapper.load(TokenEntity.class, supplierId);
        return tokenEntity.getAccessToken();*/
    }

    public ViewQuoteDynamoDBResponse insertViewQuoteDetails(B2BViewQuoteDetailEntity b2bViewQuoteDetailEntity) {

        String tableName = prefixTableName+"RideRequestDetail";
        dynamoDBMapper.save(b2bViewQuoteDetailEntity, new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build());
        ViewQuoteDynamoDBResponse viewQuoteDynamoDBResponse = new ViewQuoteDynamoDBResponse();
        viewQuoteDynamoDBResponse.setRequestId(b2bViewQuoteDetailEntity.getRequestId());
       /* viewQuoteDynamoDBResponse.setFleetId(b2bViewQuoteDetailEntity.getFleetId());
        viewQuoteDynamoDBResponse.setAccountId(b2bViewQuoteDetailEntity.getAccountId());
*/
        return viewQuoteDynamoDBResponse;
    }

    public String insertRequestOffer(B2BRequestOfferEntity smartziEngReqOffersDynamoDb) {

        String tableName = prefixTableName+"RideRequestOffer";
        dynamoDBMapper.save(smartziEngReqOffersDynamoDb, new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build());
        return smartziEngReqOffersDynamoDb.getOfferId();
    }

    public String getVehicleType(Long vehicleTypeId) {

        VehicleTypeEntity partitionKey = new VehicleTypeEntity();
        partitionKey.setVehicleId(vehicleTypeId);

        DynamoDBQueryExpression<VehicleTypeEntity> queryExpression =
                new DynamoDBQueryExpression<VehicleTypeEntity>()
                        .withHashKeyValues(partitionKey);
        String tableName = prefixTableName+"VehicleType";
        return dynamoDBMapper.query(VehicleTypeEntity.class, queryExpression,
                new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build()).get(0).getVehicleType();



       /* VehicleTypeEntity vehicleTypeEntity = dynamoDBMapper.load(VehicleTypeEntity.class,vehicleTypeId);
        return vehicleTypeEntity.getVehicleType();*/
    }

    public B2BRequestOfferEntity getSmartziEngReqOffers(String quote_id) {

        B2BRequestOfferEntity partitionKey = new B2BRequestOfferEntity();
        partitionKey.setOfferId(quote_id);
        DynamoDBQueryExpression<B2BRequestOfferEntity> queryExpression =
                new DynamoDBQueryExpression<B2BRequestOfferEntity>()
                        .withHashKeyValues(partitionKey);
        try
        {
        String tableName = prefixTableName+"RideRequestOffer";
        return dynamoDBMapper.query(B2BRequestOfferEntity.class, queryExpression,
                new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build()).get(0);
    }
        catch(IndexOutOfBoundsException  e) {
        return partitionKey;
    }
    }


    public void insertCreateBooking(CreateRideEntity createRideEntity) {
        String tableName = prefixTableName+"CreateRide";
        dynamoDBMapper.save(createRideEntity, new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build());
    }

    public void insertRideDetails(RideDetailEntity rideDetailEntity) {
        String tableName = prefixTableName+"RideDetails";
        dynamoDBMapper.save(rideDetailEntity,  new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build());
    }

    public RideDetailEntity getRideDetail(String trip_id) {

       RideDetailEntity rideDetailEntity = new RideDetailEntity();


            RideDetailEntity partitionKey = new RideDetailEntity();
            partitionKey.setTripId(trip_id);
            DynamoDBQueryExpression<RideDetailEntity> queryExpression =
                    new DynamoDBQueryExpression<RideDetailEntity>()
                            .withIndexName("tripId-index").withHashKeyValues(partitionKey).withConsistentRead(false);

       /* DynamoDBQueryExpression<RideDetailEntity> queryExpression =
                new DynamoDBQueryExpression<RideDetailEntity>()
                        .withHashKeyValues(partitionKey);*/
        try {
            String tableName = prefixTableName + "RideDetails";
            return dynamoDBMapper.query(RideDetailEntity.class, queryExpression, new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build()).get(0);
        }
        catch(IndexOutOfBoundsException  e) {
            return rideDetailEntity;
        }

    }

    public void insertCancelRide(CancelRideEntity cancelRideEntity) {
        String tableName = prefixTableName+"CancelRide";
        dynamoDBMapper.save(cancelRideEntity,  new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build());
    }

    public void insertTripStatusDetail(TripStatusEntity tripStatusEntity) {
        String tableName = prefixTableName+"RideStatus";
        dynamoDBMapper.save(tripStatusEntity,  new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build());
    }

    public CreateRideEntity getRideJsonDetail(String trip_id) {
        CreateRideEntity createRideEntity = new CreateRideEntity();
        CreateRideEntity partitionKey = new CreateRideEntity();
        partitionKey.setTripId(trip_id);
        DynamoDBQueryExpression<CreateRideEntity> queryExpression =
                new DynamoDBQueryExpression<CreateRideEntity>()
                        .withIndexName("tripId-index").withHashKeyValues(partitionKey).withConsistentRead(false);
        try {
            String tableName = prefixTableName + "CreateRide";
            return dynamoDBMapper.query(CreateRideEntity.class, queryExpression,
                    new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(tableName)).build()).get(0);
        }catch(IndexOutOfBoundsException  e) {
                return createRideEntity;
        }

    }
}
