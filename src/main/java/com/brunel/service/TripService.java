package com.brunel.service;

import com.brunel.dto.request.BrunelBookingB1Request;
import com.brunel.dto.request.CancelRequest;
import com.brunel.dto.request.InventoryBookingViewQuoteRequest;

public interface TripService {


    /**
     * Get view codes
     * @param viewQuotesRequest
     * @return
     *
     */
//    Object getInventoryQuote(ViewQuotesRequest viewQuotesRequest);

    /**
     * Cancel Booking
     * @param cancelRequest,id
     * @return
     *
     */
    Object engineCancelBooking(String id, CancelRequest cancelRequest);

    /**
     * Create Booking
     * @param bookingRequest
     * @return
     */

    Object engineBooking(BrunelBookingB1Request bookingRequest);

    /**
     * Trip Status
     * @return
     */
    Object engineTripStatus(String id);

    /**
     * Get Trip Details
     * @param id
     * @return
     */

    Object engineTripDetail(String id);

}
