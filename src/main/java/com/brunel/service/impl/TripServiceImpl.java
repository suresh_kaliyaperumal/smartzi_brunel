package com.brunel.service.impl;


import com.brunel.business.TripManagement;
import com.brunel.dto.request.BrunelBookingB1Request;
import com.brunel.dto.request.CancelRequest;
import com.brunel.dto.request.InventoryBookingViewQuoteRequest;
import com.brunel.dto.request.ViewQuotesRequest;
import com.brunel.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
public class TripServiceImpl implements TripService {

    @Autowired
    TripManagement tripManagement;

   /* @Override
    @PostMapping("/quote")
    public Object getInventoryQuote(@RequestBody ViewQuotesRequest viewQuotesRequest) {
        return tripManagement.getInventoryQuote(viewQuotesRequest);
    }*/

    @Override
    @PostMapping(value = "/cancel/trip/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    public Object engineCancelBooking(@PathVariable("id") String id, @RequestBody CancelRequest cancelRequest) {
        return tripManagement.engineCancelBooking(id,cancelRequest);
    }

    @Override
    @PostMapping(value = "/booking/trip", produces = MediaType.APPLICATION_XML_VALUE)
    public Object engineBooking(@RequestBody BrunelBookingB1Request bookingRequest) {
        return tripManagement.engineBooking(bookingRequest);
    }

    @Override
    @GetMapping("/status/trip/{tripId}")
    public Object engineTripStatus(@PathVariable("tripId") String tripId) {
        return tripManagement.engineTripStatus(tripId);
    }

    @Override
    @GetMapping("/trip/detail/{id}")
    public Object engineTripDetail(@PathVariable("id") String id) {
        return tripManagement.engineTripDetail(id);
    }


}
